## TEVO 协作 API 文档

#### 基础配置信息

> 基础域名：http://ai.tevo.online/

> ```js
> // 格式化 POST 数据
> const getPostParams = (obj) => {
>     const params = new URLSearchParams();
>     for (let key in obj) {
>       params.append(key, obj[key]);
>     }
>     return params;
> }
> // 调用
> await axios.post("https://ai.tevo.online/app/apiv1/translate", getPostParams({
> 	txt: '美女在洗澡',
> 	from: 'zh',
> 	to: 'en'
> }));
> ```
>
> 



#### 01、Google 翻译

> 地址：`/app/apiv1/translate`
>
> 方式：`post`
>
> 参数：
>
> ```json
> {
>     txt: 'xxxxx 需要翻译的文字 xxxx',
>     from: 'zh',   // 默认中文 zh，待翻译
>     to: 'en'     // 默认英文 en， 翻译结果
> }
> ```
>
> 返回：
>
> ```json
> {
>     code: 0,
>     msg: 'ok',
>     data: {
>         result: 'xxxx  翻译结果  xxxx'
>     }
> }
> ```

