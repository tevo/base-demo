const addOn = [
  // 01 - Persona
  {
    tname: "Persona",
    cname: '角色',
    children: [
      {
        tname: 'Entrepreneur',
        cname: '企业家',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Professor',
        cname: '教授',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Life coach',
        cname: '生活教练',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Marketing consultant',
        cname: '营销顾问',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Public speaker',
        cname: '公共发言人',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Social media specialist',
        cname: '社交媒体专家',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Software developer',
        cname: '软件开发人员',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Technical writer',
        cname: '技术撰稿人',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Accountant',
        cname: '会计',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Business coach',
        cname: '商务教练',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Career counselor',
        cname: '职业顾问',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Consultant',
        cname: '顾问',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Financial analyst',
        cname: '财务分析员',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Venture capitalist',
        cname: '风险资本家',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Entrepreneur',
        cname: '企业家',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'CEO',
        cname: '总裁',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'CTO',
        cname: '首席技术官',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'CFO',
        cname: '首席财务官',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'COO',
        cname: 'COO公司',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'HR professional',
        cname: '人力资源专业人员',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Marketing expert',
        cname: '市场营销专家',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Sales expert',
        cname: '销售专家',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Public relations professional',
        cname: '公共关系专业人员',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Artist',
        cname: '艺术家',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Musician',
        cname: '音乐家',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Writer',
        cname: '作家',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Psychologist',
        cname: '心理学家',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Nutritionist',
        cname: '营养学家',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Personal trainer',
        cname: '私人教练',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Dietitian',
        cname: '营养师',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Doctor',
        cname: '医生',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Lawyer',
        cname: '律师',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Engineer',
        cname: '工程师',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Architect',
        cname: '建筑师',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Scientist',
        cname: '科学家',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Professor',
        cname: '教授',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Chef',
        cname: '厨师',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Gardener',
        cname: '花匠',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Landscaper',
        cname: '园林设计师',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Scientist',
        cname: '科学家',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Graphic designer',
        cname: '平面设计师',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Interior designer',
        cname: '室内设计师',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Investment advisor',
        cname: '投资顾问',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Real estate agent',
        cname: '房地产经纪人',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Translator',
        cname: '翻译',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Travel agent',
        cname: '旅行社代理人',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Web developer',
        cname: 'Web开发人员',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Author',
        cname: '著者',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Tutor',
        cname: '家庭教师',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
      {
        tname: 'Yoga instructor',
        cname: '瑜伽教练',
        disc: "no data info",
        cinfo: '无数据信息',
        pic: ''
      },
    ]
  },
  // 02 - Audience
  {
    tname: "Audience",
    cname: '观众',
    children: [
      {
        tname: 'The bargain hunter',
        cname: '讨价还价的人',
        disc: "This customer is always on the lookout for a good deal and is willing to put in the extra effort to find the lowest price.",
        cinfo: '这种顾客总是在寻找一笔好交易，并愿意付出额外的努力来找到最低的价格。',
        pic: ''
      },
      {
        tname: 'The loyalist',
        cname: '效忠者',
        disc: "This customer is highly loyal to a particular brand or product and will continue to purchase from that company even if there are cheaper alternatives available",
        cinfo: '这类顾客对某一特定品牌或产品高度忠诚，即使有更便宜的替代品，他们也会继续从该公司购买',
        pic: ''
      },
      {
        tname: 'The early adopter',
        cname: '早期采用者',
        disc: "This customer is always the first to try out new products or technologies and is willing to pay a premium for the latest and greatest.",
        cinfo: '这类客户总是第一个尝试新产品或新技术，并愿意为最新最好的产品支付溢价。',
        pic: ''
      },
      {
        tname: 'The skeptic',
        cname: '怀疑论者',
        disc: "This customer is cautious and skeptical of marketing claims and needs to see hard evidence before making a purchase.",
        cinfo: '这类客户对市场宣传持谨慎和怀疑态度，需要在购买前看到确凿的证据。',
        pic: ''
      },
      {
        tname: 'The impulse buyer',
        cname: '冲动的买家',
        disc: "This customer is prone to making impulsive purchases, often driven by emotions or a sense of urgency.",
        cinfo: '这类顾客容易冲动购物，通常受情绪或紧迫感的驱使。',
        pic: ''
      },
      {
        tname: 'The trend follower',
        cname: '潮流追随者',
        disc: "This customer is always looking to stay up-to-date with the latest trends and will often purchase products that are popular or fashionable.",
        cinfo: '这类顾客总是希望跟上最新的潮流，经常会购买流行或时尚的产品。',
        pic: ''
      },
      {
        tname: 'The quality seeker',
        cname: '质量探索者',
        disc: "This customer places a high value on quality and is willing to pay a premium for products that are well-made and durable.",
        cinfo: '这类顾客重视质量，愿意为做工精良、经久耐用的产品支付高价。',
        pic: ''
      },
      {
        tname: 'The convenience seeker',
        cname: '追求便利的人',
        disc: "This customer values convenience above all else and is willing to pay a higher price for products or services that make their life easier.",
        cinfo: '这类顾客最看重的是便利性，他们愿意为那些让他们的生活更轻松的产品或服务支付更高的价格。',
        pic: ''
      },
      {
        tname: 'The environmentalist',
        cname: '环保主义者',
        disc: "This customer is environmentally conscious and is willing to pay a premium for products that are eco-friendly or sustainable.",
        cinfo: '这类顾客有环保意识，愿意为环保或可持续的产品支付溢价。',
        pic: ''
      },
      {
        tname: 'The health-conscious customer',
        cname: '注重健康的客户',
        disc: "This customer is focused on living a healthy lifestyle and is willing to pay a higher price for products that align with their values.",
        cinfo: '这类客户注重健康的生活方式，愿意为符合他们价值观的产品支付更高的价格。',
        pic: ''
      },
      {
        tname: 'The luxury seeker',
        cname: '追求奢华的人',
        disc: "This customer is attracted to luxury brands and is willing to pay a high price for products that exude wealth and status.",
        cinfo: '这类消费者被奢侈品牌所吸引，愿意为彰显财富和地位的产品支付高价。',
        pic: ''
      },
      {
        tname: 'The tech-savvy customer',
        cname: '精通技术的客户',
        disc: "This customer is highly knowledgeable about technology and is willing to pay a premium for the latest gadgets and innovations.",
        cinfo: '这类顾客对技术非常了解，愿意为最新的小玩意和创新产品支付溢价。',
        pic: ''
      },
      {
        tname: 'The price-sensitive customer',
        cname: '对价格敏感的客户',
        disc: "This customer is always looking for the best value for their money and will carefully compare prices before making a purchase.",
        cinfo: '这种顾客总是在寻找物有所值的东西，在购买之前会仔细比较价格。',
        pic: ''
      },
      {
        tname: 'The price-insensitive customer',
        cname: '对价格不敏感的客户',
        disc: "This customer is willing to pay a high price for products or services, regardless of cost.",
        cinfo: '这种顾客愿意不计成本地为产品或服务支付高价。',
        pic: ''
      },
      {
        tname: 'The DIY customer',
        cname: 'DIY客户',
        disc: "This customer prefers to do things themselves and is willing to pay a higher price for products that allow them to do so.",
        cinfo: '这类客户更喜欢自己动手，并且愿意为允许他们这样做的产品支付更高的价格。',
        pic: ''
      },
      {
        tname: 'The time-poor customer',
        cname: '时间差的客户',
        disc: "This customer is short on time and is willing to pay a premium for products or services that save them time.",
        cinfo: '这类客户时间紧迫，愿意为节省时间的产品或服务支付额外费用。',
        pic: ''
      },
      {
        tname: 'The risk-averse customer',
        cname: '规避风险的客户',
        disc: "This customer is cautious and avoids taking risks, preferring to pay a higher price for products that are tried and tested.",
        cinfo: '这类客户非常谨慎，不愿冒险，更愿意为经过测试的产品支付更高的价格。',
        pic: ''
      },
      {
        tname: 'The risk-taking customer',
        cname: '敢于冒险的客户',
        disc: "This customer is willing to take risks and try new things, even if it means paying a higher price.",
        cinfo: '这类客户愿意冒险并尝试新事物，即使这意味着要付出更高的代价。',
        pic: ''
      },
      {
        tname: 'The emotional customer',
        cname: '情绪化的客户',
        disc: "This customer makes purchasing decisions based on emotions and is often swayed by emotional marketing tactics.",
        cinfo: '这类顾客会根据情绪做出购买决定，并经常受到情绪营销策略的影响。',
        pic: ''
      },
      {
        tname: 'The rational customer',
        cname: '理性的客户',
        disc: "This customer makes purchasing decisions based on logical considerations and is more resistant to emotional marketing tactics.",
        cinfo: '这类顾客会基于逻辑考虑做出购买决定，对情感营销策略更有抵抗力。',
        pic: ''
      },
    ]
  },
  // 03 - Tone
  {
    tname: "Tone",
    cname: '色调',
    children: [
      {
        tname: 'Informative',
        cname: '提供有用信息的',
        disc: "Writing that is designed to inform or educate the reader.",
        cinfo: '为告知或教育读者而写的文章。',
        pic: ''
      },
      {
        tname: 'Persuasive',
        cname: '有说服力的',
        disc: "Writing designed to convince the reader to take a particular action or adopt a certain viewpoint.",
        cinfo: '旨在说服读者采取某种行动或采取某种观点的文章。',
        pic: ''
      },
      {
        tname: 'Descriptive',
        cname: '描写的',
        disc: "Writing that is designed to vividly describe people, places, objects, or events.",
        cinfo: '生动地描述人物、地点、物体或事件的文字。',
        pic: ''
      },
      {
        tname: 'Emotional',
        cname: '感情的',
        disc: "Writing that is designed to evoke strong feelings or emotions in the reader.",
        cinfo: '旨在唤起读者强烈感情或情感的文章。',
        pic: ''
      },
      {
        tname: 'Inspirational',
        cname: '启发灵感的',
        disc: "Writing that is designed to inspire or motivate the reader.",
        cinfo: '旨在启发或激励读者的文章。',
        pic: ''
      },
      {
        tname: 'Humorous',
        cname: '幽默的',
        disc: "Writing that is intended to be funny or amusing.",
        cinfo: '搞笑的:旨在搞笑或逗乐的写作',
        pic: ''
      },
      {
        tname: 'Sarcastic',
        cname: '讽刺的',
        disc: "Writing that is intended to be ironic or mocking, often in a humorous way.",
        cinfo: '讽刺性写作:意在讽刺或嘲弄的写作，通常以幽默的方式',
        pic: ''
      },
      {
        tname: 'Poetic',
        cname: '诗意的',
        disc: "Writing that uses language in a rhythmic or expressive way, often with a focus on imagery or emotion.",
        cinfo: '以有节奏或富有表现力的方式使用语言的写作，通常注重意象或情感',
        pic: ''
      },
      {
        tname: 'Satirical',
        cname: '讽刺',
        disc: "Writing that uses humor or irony to criticize or ridicule something or someone.",
        cinfo: '用幽默或讽刺来批评或嘲笑某事或某人的文章。',
        pic: ''
      },
      {
        tname: 'Intense',
        cname: '强烈的',
        disc: "Writing that is designed to be powerful or intense, often with a strong emotional impact.",
        cinfo: '强有力的或激烈的写作，通常带有强烈的情感冲击。',
        pic: ''
      },
      {
        tname: 'Thoughtful',
        cname: '周到的',
        disc: "Writing that is designed to be reflective or contemplative, often with a focus on ideas or concepts.",
        cinfo: '以反思或沉思为目的的写作，通常侧重于思想或概念。',
        pic: ''
      },
      {
        tname: 'Elegant',
        cname: '优雅的',
        disc: "Writing that is designed to be graceful or refined, often with a focus on beauty or refinement.",
        cinfo: '优美优美的文字优美优美的文字，通常注重美或优美',
        pic: ''
      },
      {
        tname: 'Passionate',
        cname: '热情的',
        disc: "Writing that is designed to be full of energy and emotion, often with a focus on personal beliefs or values.",
        cinfo: '被设计成充满活力和情感的写作，通常关注个人信仰或价值观。',
        pic: ''
      },
      {
        tname: 'Provocative',
        cname: '挑衅的',
        disc: "Writing that is designed to challenge or provoke the reader, often with a controversial or unconventional viewpoint.",
        cinfo: '标新立异的文章:旨在挑战或刺激读者的文章，通常采用有争议的或非传统的观点',
        pic: ''
      },
      {
        tname: 'Sensual',
        cname: '性感的',
        disc: "Writing that is designed to evoke the senses or emotions, often with a focus on physical sensations or pleasure.",
        cinfo: '写作:旨在唤起感官或情感的写作，通常侧重于身体的感觉或愉悦',
        pic: ''
      },
      {
        tname: 'Mystical',
        cname: '神秘的',
        disc: "Writing that is designed to evoke a sense of mystery or wonder, often with a focus on the supernatural or the unknown.",
        cinfo: '神秘小说:旨在唤起一种神秘感或惊奇感的作品，通常关注超自然或未知的事物',
        pic: ''
      },
      {
        tname: 'Romantic',
        cname: '浪漫的',
        disc: "Writing that is designed to evoke feelings of love or romance, often with a focus on emotion or intimacy.",
        cinfo: '爱情小说:旨在唤起爱情或浪漫感觉的作品，通常以情感或亲密为重点',
        pic: ''
      },
      {
        tname: 'Sensitive',
        cname: '敏感的',
        disc: "Writing that is designed to be sensitive or empathetic, often with a focus on understanding or compassion.",
        cinfo: '以敏感或同情为目的的写作，通常侧重于理解或同情。',
        pic: ''
      },
      {
        tname: 'Nostalgic',
        cname: '怀旧',
        disc: "Writing designed to evoke feelings of longing or nostalgia, often focusing on the past or on lost opportunities.",
        cinfo: '怀旧文章:旨在唤起渴望或怀旧之情的文章，通常侧重于过去或失去的机会',
        pic: ''
      },
      {
        tname: 'Sensational',
        cname: '轰动的',
        disc: "Writing that is designed to shock or surprise the reader, often with a focus on spectacular or dramatic events.",
        cinfo: '旨在使读者震惊或惊讶的文章，通常关注壮观或戏剧性的事件。',
        pic: ''
      },
      {
        tname: 'Passionless',
        cname: '没有激情',
        disc: "Writing that is designed to be unemotional or detached, often focusing on facts or objective analysis.",
        cinfo: '不带感情色彩或不带感情色彩的文章，通常侧重于事实或客观分析。',
        pic: ''
      },
      {
        tname: 'Mysterious',
        cname: '秘密的',
        disc: "Writing designed to be enigmatic or obscure, often focusing on secrecy or hidden meanings.",
        cinfo: '神秘的或晦涩的作品，通常侧重于秘密或隐藏的含义',
        pic: ''
      },
      {
        tname: 'Impersonal',
        cname: '不近人情的',
        disc: "Writing that is designed to be objective or detached, often with a focus on impersonal subjects or concepts.",
        cinfo: '客观或超然的写作:旨在客观或超然的写作，通常侧重于客观的主题或概念',
        pic: ''
      },
      {
        tname: 'Dreamy',
        cname: '梦幻般的',
        disc: "Writing that is designed to evoke feelings of dreaming or imagination, often with a focus on the surreal or the ethereal.",
        cinfo: '梦幻小说:旨在唤起梦境或想象感觉的作品，通常以超现实或虚幻为重点',
        pic: ''
      },
      {
        tname: 'Imaginative',
        cname: '富有想象力的',
        disc: "Writing that is designed to be creative or imaginative, often focusing on unique or unconventional ideas.",
        cinfo: '富有创造性或想象力的作品，通常集中在独特或非传统的想法上。',
        pic: ''
      },
      {
        tname: 'Melodic',
        cname: '旋律的',
        disc: "Writing that is designed to be rhythmic or musical, often with a focus on language and sound.",
        cinfo: '韵律性或音乐性的写作，通常注重语言和声音。',
        pic: ''
      },
      {
        tname: 'Experimental',
        cname: '实验的',
        disc: "Writing that is designed to be experimental or unconventional, often with a focus on testing the boundaries of language or form.",
        cinfo: '实验性写作:设计为实验性或非传统的写作，通常侧重于测试语言或形式的边界',
        pic: ''
      },
      {
        tname: 'Lighthearted',
        cname: '轻松愉快的',
        disc: "Writing that is designed to be playful or lighthearted, often incorporating humor, levity, and a sense of fun to engage the reader.",
        cinfo: '以好玩或轻松为目的的写作，通常结合幽默、轻浮和一种有趣的感觉来吸引读者。',
        pic: ''
      },
      {
        tname: 'Enthusiastic',
        cname: '热情的',
        disc: "Writing that is designed to be full of energy and enthusiasm, often with a focus on excitement or positivity.",
        cinfo: '写作的目的是充满活力和热情，通常集中在兴奋或积极。',
        pic: ''
      },
      {
        tname: 'Soothing',
        cname: '舒缓的',
        disc: "Writing that is designed to be calming or soothing, often focusing on relaxation or tranquility.",
        cinfo: '旨在使人平静或舒缓的文章，通常侧重于放松或宁静。',
        pic: ''
      },
      {
        tname: 'Eerie',
        cname: '诡异的',
        disc: "Writing designed to be unsettling or creepy, often focusing on the supernatural or the unknown.",
        cinfo: '惊悚小说:被设计成令人不安或令人毛骨悚然的作品，通常关注超自然或未知的事物',
        pic: ''
      },
      {
        tname: 'Whimsical',
        cname: '异想天开的',
        disc: "Writing that is designed to be fanciful or playful, often with a focus on imagination or fantasy.",
        cinfo: '幻想写作:以幻想或好玩为目的的写作，通常侧重于想象或幻想',
        pic: ''
      },
      {
        tname: 'Mournful',
        cname: '悲哀的',
        disc: "Writing that is designed to evoke feelings of sadness or loss, often with a focus on grief or sorrow.",
        cinfo: '悲伤的文章:旨在唤起悲伤或失落的感觉的文章，通常以悲伤或遗憾为重点',
        pic: ''
      },
      {
        tname: 'Sorrowful',
        cname: '悲伤的',
        disc: "Writing that is designed to evoke feelings of sadness or melancholy, often with a focus on grief or regret.",
        cinfo: '悲伤:旨在唤起悲伤或忧郁情绪的文章，通常以悲伤或遗憾为重点',
        pic: ''
      },
      {
        tname: 'Joyful',
        cname: '快乐',
        disc: "Writing that is designed to evoke feelings of happiness or joy, often with a focus positive experiences or emotions.",
        cinfo: '写作旨在唤起幸福或快乐的感觉，通常以积极的经历或情绪为重点。',
        pic: ''
      },
      {
        tname: 'Exhilarating',
        cname: '令人兴奋的',
        disc: "Writing that is designed to be exciting or exhilarating, often with a focus on energy or enthusiasm.",
        cinfo: '令人兴奋或振奋的写作，通常注重精力或热情。',
        pic: ''
      },
      {
        tname: 'Uplifting',
        cname: '提升',
        disc: "Writing that is designed to be positive or inspiring, often with a focus on hope or optimism.",
        cinfo: '积极的或鼓舞人心的写作，通常以希望或乐观为重点。',
        pic: ''
      },
      {
        tname: 'Reflective',
        cname: '反思的',
        disc: "Writing that is designed to be introspective or contemplative, often with a focus on personal experience or self-examination.",
        cinfo: '以自省或沉思为目的的写作，通常侧重于个人经历或自我反省。',
        pic: ''
      },
      {
        tname: 'Powerful',
        cname: '强大的',
        disc: "Writing that is designed to be strong or impactful, often with a focus on emotion or intensity.",
        cinfo: '强烈的或有影响力的写作，通常侧重于情感或强度。',
        pic: ''
      },
      {
        tname: 'Melancholic',
        cname: '忧郁的',
        disc: "Writing that is designed to evoke feelings of sadness or melancholy, often with a focus on reflection or introspection.",
        cinfo: '哀思:旨在唤起悲伤或忧郁情绪的文章，通常以反思或自省为重点',
        pic: ''
      },
      {
        tname: 'Melodramatic',
        cname: '戏剧性的',
        disc: "Writing that is designed to be dramatic or over-the-top, often with a focus on exaggerated emotions or actions.",
        cinfo: '夸张的写作:被设计成戏剧性的或夸张的写作，通常侧重于夸张的情感或行为',
        pic: ''
      },
      {
        tname: 'Energetic',
        cname: '精力充沛的',
        disc: "Writing that is designed to be full of energy or enthusiasm, often with a focus on excitement or positivity.",
        cinfo: '充满活力或热情的写作，通常以兴奋或积极为重点。',
        pic: ''
      },
      {
        tname: 'Heartwarming',
        cname: '温暖人心',
        disc: "Writing that is designed to evoke feelings of warmth or love, often with a focus on positive emotions or relationships.",
        cinfo: '温馨的文章:旨在唤起温暖或爱的感觉的文章，通常侧重于积极的情绪或关系',
        pic: ''
      },
      {
        tname: 'Witty',
        cname: '言辞诙谐的',
        disc: "Writing that is humorous and clever, often with a sharp or ironic edge.",
        cinfo: '幽默风趣的作品幽默而聪明的作品，常带有尖锐或讽刺的意味',
        pic: ''
      },
    ]
  },
  // 04 - Style
  {
    tname: "Style",
    cname: '风格',
    children: [
      {
        tname: 'Formal, academic style',
        cname: '正式、学术风格',
        disc: "a style that is formal and follows the conventions of academic writing. Using a formal, academic style can make the final product seem more scholarly and credible.",
        cinfo: '遵循学术写作惯例的正式文体。使用正式的学术风格可以使最终的产品看起来更学术和可信。',
        pic: ''
      },
      {
        tname: 'Casual, conversational style',
        cname: '休闲、谈话风格',
        disc: "a style that is informal and uses language and structure similar to spoken conversation. Using a casual, conversational style can make the final product seem more friendly and approachable.",
        cinfo: '一种非正式的文体，使用类似于口语的语言和结构。使用随意的对话风格可以使最终产品看起来更友好，更平易近人。',
        pic: ''
      },
      {
        tname: 'Persuasive, sales style',
        cname: '说服力强，销售风格',
        disc: "a style that is designed to persuade the reader to take a certain action. Using a persuasive, sales style can make the final product more effective at selling a product or idea.",
        cinfo: '一种旨在说服读者采取某种行动的文体。使用有说服力的销售风格可以使最终产品更有效地销售产品或想法。',
        pic: ''
      },
      {
        tname: 'Creative, literary style',
        cname: '富有创意的文学风格',
        disc: "a style that is imaginative and artistic, often using figurative language and other literary devices.  Using a creative, literary style can make the final product more engaging and memorable.",
        cinfo: '一种富有想象力和艺术性的文体，常使用比喻语言和其他文学手法。使用创造性的文学风格可以使最终产品更吸引人，更令人难忘。',
        pic: ''
      },
      {
        tname: 'Professional, business style',
        cname: '专业，商务风格',
        disc: "a style that is formal and follows the conventions of business writing.  Using a professional, business style can make the final product seem more polished and professional.",
        cinfo: '一种遵循商务写作惯例的正式文体。使用专业的商务风格可以使最终的产品看起来更加精致和专业。',
        pic: ''
      },
      {
        tname: 'Descriptive, vivid style',
        cname: '描述性、生动的风格',
        disc: "a style that uses detailed, sensory language to create vivid images in the reader's mind.  Using a descriptive, vivid style can make the final product more engaging and immersive.",
        cinfo: '一种运用细节的、感性的语言在读者脑海中创造生动形象的文体。使用描述性、生动的风格可以使最终产品更具吸引力和沉浸感。',
        pic: ''
      },
      {
        tname: 'Emotional, dramatic style',
        cname: '情绪化、戏剧性的风格',
        disc: "a style that uses emotional language and rhetorical devices to create a strong emotional response in the reader.  Using an emotional, dramatic style can make the final product more impactful and persuasive.",
        cinfo: '一种使用情感语言和修辞手段来引起读者强烈情感反应的文体。使用情绪化、戏剧性的风格可以使最终产品更具影响力和说服力。',
        pic: ''
      },
      {
        tname: 'Humorous, witty style',
        cname: '幽默、诙谐的风格',
        disc: "a style that uses humor and wit to entertain the reader.  Using a humorous, witty style can make the final product more enjoyable and memorable.",
        cinfo: '一种用幽默和机智来娱乐读者的文体。使用幽默、诙谐的风格可以使最终的产品更令人愉快和难忘。',
        pic: ''
      },
      {
        tname: 'Formal, legal style',
        cname: '正式、合法的风格',
        disc: "a style that is formal and follows the conventions of legal writing.  Using a formal, legal style can make the final product seem more precise and reliable.",
        cinfo: '正式文体一种遵循法律写作惯例的正式文体使用正式的、合法的文体可以使最终的产品看起来更精确、更可靠。',
        pic: ''
      },
      {
        tname: 'Informal, chatty style',
        cname: '非正式、健谈的风格',
        disc: "a style that is informal and uses language and structure similar to spoken conversation.  Using an informal, chatty style can make the final product seem more friendly and approachable.",
        cinfo: '一种非正式的文体，使用类似于口语的语言和结构。使用非正式的、健谈的风格可以使最终的产品看起来更友好、更平易近人。',
        pic: ''
      },
      {
        tname: 'Formal, bureaucratic style',
        cname: '正式、官僚作风',
        disc: "a style that is formal and follows the conventions of bureaucratic writing.  Using a formal, bureaucratic style can make the final product seem more official and reliable.",
        cinfo: '正式文体遵循官僚文体的惯例的正式文体。使用正式的、官僚的风格可以使最终的产品看起来更正式、更可靠。',
        pic: ''
      },
      {
        tname: 'Poetic, metaphorical style',
        cname: '诗意、隐喻的风格',
        disc: "a style that uses figurative language and other poetic devices to create vivid imagery and convey meaning in creative ways.  Using a poetic, metaphorical style can make the final product more artistic and evocative.",
        cinfo: '用比喻的语言和其他诗意的手段来创造生动的形象，并以创造性的方式传达意义的一种风格。使用诗意、隐喻的风格可以使最终产品更具艺术性和唤起性。',
        pic: ''
      },
      {
        tname: 'Concise, bullet-point style',
        cname: '简洁、要点式',
        disc: "a style that presents information in short, concise points, often using bullet points or numbered lists.  Using a concise, bullet-point style can make the final product easier to read and understand.",
        cinfo: '一种以短小精练的要点来表达信息的文体，通常使用项目符号或编号列表。使用简洁、重点的风格可以使最终产品更容易阅读和理解。',
        pic: ''
      },
      {
        tname: 'Elaborate, ornate style',
        cname: '精致、华丽的风格',
        disc: "a style that uses elaborate language and decorative elements to add flair and elegance to the final product.  Using an elaborate, ornate style can make the final product more sophisticated and impressive.",
        cinfo: '一种使用精致的语言和装饰元素为最终产品增添风格和优雅的风格。使用精致、华丽的风格可以使最终产品更加复杂和令人印象深刻。',
        pic: ''
      },
      {
        tname: 'Direct, no-nonsense style',
        cname: '直接、严肃的风格',
        disc: "a style that is straightforward and gets straight to the point.  Using a direct, no-nonsense style can make the final product more efficient and effective.",
        cinfo: '一种直截了当、直奔主题的风格。使用直接、严肃的风格可以使最终产品更有效率和有效。',
        pic: ''
      },
      {
        tname: 'Playful, teasing style',
        cname: '嬉闹、调侃的风格',
        disc: "a style that uses humor and teasing to engage the reader.  Using a playful, teasing style can make the final product more enjoyable and lighthearted.",
        cinfo: '一种用幽默和逗趣来吸引读者的文体。使用一种俏皮的、逗趣的风格可以使最终的产品更令人愉快和轻松。',
        pic: ''
      },
      {
        tname: 'Formal, invitation style',
        cname: '正式、邀请式',
        disc: "a style that follows the conventions of formal invitations.  Using a formal, invitation style can make the final product more polished and proper.",
        cinfo: '遵循正式邀请惯例的一种风格。使用正式的邀请风格可以使最终的作品更精致、更得体。',
        pic: ''
      },
      {
        tname: 'Formal, condolence style',
        cname: '正式的吊唁风格',
        disc: "a style that follows the conventions of expressing condolences.  Using a formal, condolence style can make the final product more respectful and appropriate.",
        cinfo: '吊唁:遵循哀悼惯例的一种方式使用正式的，哀悼的风格可以使最终的产品更尊重和适当。',
        pic: ''
      },
      {
        tname: 'Formal, congratulations style',
        cname: '正式的祝贺风格',
        disc: "a style that follows the conventions of expressing congratulations.  Using a formal, congratulations style can make the final product more gracious and sincere.",
        cinfo: '祝贺词:遵循祝贺惯例的一种文体使用正式的，祝贺的风格可以使最终的产品更亲切和真诚。',
        pic: ''
      },
      {
        tname: 'Formal, thank-you style',
        cname: '正式的感谢风格',
        disc: "a style that follows the conventions of expressing gratitude.  Using a formal, thank-you style can make the final product more sincere and appreciative.",
        cinfo: '一种遵循表达感激的惯例的风格。使用正式的感谢方式可以使最终的产品更真诚和欣赏。',
        pic: ''
      },
    ]
  },
  // 05 - Voice
  {
    tname: "Voice",
    cname: '声音',
    children: [
      {
        tname: 'A high-pitched, energetic voice',
        cname: '高亢有力的声音',
        disc: "a voice that is higher in pitch and full of energy and enthusiasm. Using a high-pitched, energetic voice can add excitement and enthusiasm to a message.",
        cinfo: '高音调的声音，充满活力和热情的声音。使用高音调，充满活力的声音可以增加兴奋和热情的信息。',
        pic: ''
      },
      {
        tname: 'A deep, gravelly voice',
        cname: '低沉沙哑的声音',
        disc: "a voice that is low in pitch and rough or gravelly in quality. Using a deep, gravelly voice can add gravity and authority to a message.",
        cinfo: '低沉的声音音调低而粗糙或沙哑的声音使用低沉、沙哑的声音可以增加信息的重力和权威。',
        pic: ''
      },
      {
        tname: 'A soft, gentle voice',
        cname: '轻柔的声音',
        disc: "a quiet, soothing voice. Using a soft, gentle voice can convey warmth and compassion.",
        cinfo: '一个安静、舒缓的声音。使用柔和、温柔的声音可以传达温暖和同情。',
        pic: ''
      },
      {
        tname: 'A nasally voice',
        cname: '鼻音',
        disc: "a voice that sounds as though it is coming through the nose. A nasally voice can be annoying or off-putting to some listeners.",
        cinfo: '听起来像是从鼻子里传出来的声音。带鼻音的声音可能会让一些听众感到讨厌或反感。',
        pic: ''
      },
      {
        tname: 'An accent',
        cname: '口音',
        disc: "a voice with a distinct regional or foreign inflection. An accent can add flavor and character to a message, but it can also make it harder for some listeners to understand.",
        cinfo: '带有明显地区或外国口音的声音口音可以为信息增添风味和个性，但也会让一些听众更难理解。',
        pic: ''
      },
      {
        tname: 'A monotone voice',
        cname: '单调的声音',
        disc: "a voice that speaks in a flat, uninflected tone. Using a monotone voice can make a message seem unemotional or dull.",
        cinfo: '说话的声音:用平坦的、不变化的语调说话的声音使用单调的声音会使信息看起来缺乏情感或沉闷。',
        pic: ''
      },
      {
        tname: 'A singsong voice',
        cname: '歌声',
        disc: "a voice that speaks in a rhythmic, musical pattern. Using a singsong voice can add playfulness or whimsy to a message.",
        cinfo: '有节奏的声音:以有节奏的音乐形式说话的声音使用唱歌的声音可以给信息增加趣味性或奇思妙想。',
        pic: ''
      },
      {
        tname: 'A raspy voice',
        cname: '刺耳的声音',
        disc: "a voice that is rough or hoarse, as if from throat irritation. A raspy voice can add intensity or emotion to a message.",
        cinfo: '嘶哑的声音粗糙或沙哑的声音，好象是由于喉咙发炎而发出的沙哑的声音可以增加信息的强度或情感。',
        pic: ''
      },
      {
        tname: 'A husky voice',
        cname: '沙哑的声音',
        disc: "a voice that is deep and slightly rough. A husky voice can add sexiness or mystery to a message.",
        cinfo: '低沉而略粗糙的声音。沙哑的声音可以给信息增加性感或神秘感。',
        pic: ''
      },
      {
        tname: 'A booming voice',
        cname: '洪亮的声音',
        disc: "a voice that is loud and commanding. Using a booming voice can make a message seem more assertive or confident.",
        cinfo: '威严的声音响亮而威严的声音使用洪亮的声音可以使信息看起来更果断或自信。',
        pic: ''
      },
      {
        tname: 'A smooth, silky voice',
        cname: '流畅的嗓音',
        disc: "a voice that is soft and pleasant to listen to. Using a smooth, silky voice can make a message seem more sophisticated or charming.",
        cinfo: '柔和而悦耳的声音。使用流畅、柔滑的声音可以使信息看起来更复杂或迷人。',
        pic: ''
      },
      {
        tname: 'A whispery voice',
        cname: '轻声细语',
        disc: "a voice that is quiet and speaks in a low, hushed tone. Using a whispery voice can add intimacy or secrecy to a message.",
        cinfo: '一种安静的声音，用低沉的声音说话。使用耳语的声音可以增加信息的亲近感或保密性。',
        pic: ''
      },
      {
        tname: 'An upbeat, cheerful voice',
        cname: '乐观、愉快的声音',
        disc: "a voice that is full of cheer and positivity. Using an upbeat, cheerful voice can make a message seem more positive or friendly.",
        cinfo: '充满欢乐和积极的声音。使用乐观、愉快的声音可以使信息看起来更积极或友好。',
        pic: ''
      },
      {
        tname: 'A deadpan voice',
        cname: '面无表情的声音',
        disc: "a voice that speaks in a flat, expressionless tone. Using a deadpan voice can add humor or irony to a message.",
        cinfo: '无表情的声音:用平淡、无表情的语气说话的声音使用面无表情的声音可以增加幽默或讽刺的信息。',
        pic: ''
      },
      {
        tname: 'A sarcastic voice',
        cname: '讽刺的声音',
        disc: "a voice that speaks with irony or sarcasm. Using a sarcastic voice can add humor or disbelief to a message.",
        cinfo: '讽刺的声音:带着讽刺或讽刺说话的声音使用讽刺的声音可以增加幽默或难以置信的信息。',
        pic: ''
      },
      {
        tname: 'A mocking voice',
        cname: '嘲弄的声音',
        disc: "a voice that speaks with disdain or ridicule. Using a mocking voice can convey contempt or disrespect.",
        cinfo: '轻蔑或嘲笑的声音使用嘲弄的声音可以传达轻蔑或不尊重。',
        pic: ''
      },
      {
        tname: 'A grave, serious voice',
        cname: '严重、严肃的声音',
        disc: "a voice that is somber and earnest. Using a grave, serious voice can make a message seem more important or sincere.",
        cinfo: '一种忧郁而诚挚的声音。使用严肃、严肃的声音可以使信息看起来更重要或更真诚。',
        pic: ''
      },
      {
        tname: 'An excited, enthusiastic voice',
        cname: '激动、热情的声音',
        disc: "a voice that is full of excitement and enthusiasm. Using an excited, enthusiastic voice can add excitement and energy to a message.",
        cinfo: '充满兴奋和热情的声音。使用激动、热情的声音可以给信息增添兴奋和活力。',
        pic: ''
      },
      {
        tname: 'A worried, anxious voice',
        cname: '忧心忡忡的声音',
        disc: "a voice that is filled with worry or nervousness. Using a worried, anxious voice can convey concern or unease.",
        cinfo: '充满忧虑或紧张的声音使用担心、焦虑的声音可以表达担忧或不安。',
        pic: ''
      },
      {
        tname: 'A calm, soothing voice',
        cname: '平静、舒缓的声音',
        disc: "a voice that is reassuring and calming. Using a calm, soothing voice can help to ease tension or anxiety in a message.",
        cinfo: '安抚的声音安抚和镇静的声音使用平静、舒缓的声音可以帮助缓解信息中的紧张或焦虑。',
        pic: ''
      },
    ]
  }
]