const dataList = [
    {
        //第一类目第一项Color & Lighting
        tname: 'Color&Lighting',
        cname: '颜色和灯光',
        children: [
            {
                tname: 'Types',
                cname: '类型',
                children: [
                    {
                        tname: 'Spotlight',
                        cname: '聚光灯',
                        disc: 'focused, intense beam of light',
                        cinfo: '聚焦的、强烈的光束',
                        pic: 'http://qnimgstore.ywfun.cn/648683a116ebb.jpg'
                    },
                    {
                        tname: 'Front Light',
                        cname: '前灯',
                        disc: 'Direct, illuminating source highlighting the front-facing aspect.',
                        cinfo: '直接光源，突出正面。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/Hero-Home-page-new-1.png'
                    },
                    {
                        tname: 'Halfrear Light',
                        cname: '半尾灯',
                        disc: 'Rear-facing illumination for enhanced visibility from a partial angle.',
                        cinfo: '后向照明，从局部角度增强可视性。',
                        pic: 'http://qnimgstore.ywfun.cn/64780860cf89c.jpg'
                    },
                    {
                        tname: 'Back Light',
                        cname: '背光灯',
                        disc: 'light from behind the subject',
                        cinfo: '拍摄对象背后的光线',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/Hero-Home-page-new-1.png'
                    },
                    {
                        tname: 'Rim Light',
                        cname: '轮廓光',
                        disc: 'Accentuating glow outlining the edges, creating depth and separation.',
                        cinfo: '突出发光勾勒边缘，创造深度和分离。',
                        pic: 'http://qnimgstore.ywfun.cn/647807a3bea1c.jpg'
                    },
                    {
                        tname: 'Flood Light',
                        cname: '泛光灯',
                        disc: 'Wide-reaching, powerful illumination for broad coverage and visibility.',
                        cinfo: '覆盖范围广，照明效果强，覆盖范围广且能见度高。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/Hero-Home-page-new-1.png'
                    },
                    {
                        tname: 'Marquee',
                        cname: '选框',
                        disc: 'Eye-catching, decorative light commonly used in signs and displays.',
                        cinfo: '引人注目的装饰灯，常用于标志和展示中。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Bright',
                        cname: '光线充足的',
                        disc: 'Intense, luminous illumination with high levels of brightness.',
                        cinfo: '强烈明亮的照明，亮度高。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Ultrabright',
                        cname: '超亮',
                        disc: 'Exceptionally intense and dazzling illumination, surpassing standard brightness levels.',
                        cinfo: '异常强烈和耀眼的照明，超过标准亮度水平。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/Intro-Home-page-new.png'
                    },
                    {
                        tname: 'Crepuscular Rays',
                        cname: '云隙光',
                        disc: 'sunbeams shining through clouds.',
                        cinfo: '阳光透过云层照耀。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Candle Light',
                        cname: '蜡烛灯',
                        disc: 'dim yellow light from candles.',
                        cinfo: '蜡烛发出的暗淡的黄光。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Fluorescent',
                        cname: '荧光的',
                        disc: 'long, thin bulbs that emit bright white light.',
                        cinfo: '细长的灯泡，发出明亮的白光',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Fire Light',
                        cname: '消防照明灯',
                        disc: 'light and heat from combustion.',
                        cinfo: '燃烧产生的光和热。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Sun Light',
                        cname: '太阳光',
                        disc: 'light from the sun.',
                        cinfo: '来自太阳的光。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Electric Arc',
                        cname: '电弧',
                        disc: 'bright electrical discharge.',
                        cinfo: '明亮的放电。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Edison Bulb',
                        cname: '爱迪生灯泡',
                        disc: 'vintage-looking bulb with warm light.',
                        cinfo: '外观复古的灯泡，光线温暖。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Natural Lighting',
                        cname: '自然光',
                        disc: 'light from natural sources, such as the sun.',
                        cinfo: '来自自然来源的光，如太阳。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Nightclub Lighting',
                        cname: '夜总会照明',
                        disc: 'colorful, dynamic lighting for nightlife entertainment.',
                        cinfo: '为夜生活娱乐提供丰富多彩的动态照明。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Ultraviolet',
                        cname: '紫外线',
                        disc: 'electromagnetic radiation with shorter wavelengths than visible light.',
                        cinfo: '波长比可见光短的电磁辐射。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Strobe Light',
                        cname: '频闪灯',
                        disc: 'flashing light at regular intervals.',
                        cinfo: '每隔一段时间闪烁一次。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Glowstick',
                        cname: '荧光棒',
                        disc: 'chemical light stick used in dark environments.',
                        cinfo: '黑暗环境中使用的化学光棍。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Neon Lamp',
                        cname: '霓虹灯',
                        disc: 'long, bright lamps that emit various colors.',
                        cinfo: '发出各种颜色的长而明亮的灯。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Moonlight',
                        cname: '月光',
                        disc: 'light reflected from the moon.',
                        cinfo: '从月球反射的光。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Lava Glow',
                        cname: '熔岩发光',
                        disc: 'red-orange light from molten lava.',
                        cinfo: '熔岩发出的橙红色光。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Glowing',
                        cname: '发光的',
                        disc: 'soft light emanating from an object.',
                        cinfo: '从物体发出的柔和光线。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    }
                ],
            }, {
                tname: 'Displays',
                cname: '显示',
                children: [
                    {
                        tname: 'CRT',
                        cname: '阴极射线管',
                        disc: 'Flickering scan lines producing retro visual nostalgia.',
                        cinfo: '闪烁的扫描线产生复古的视觉怀旧',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'LCD',
                        cname: '液晶显示',
                        disc: 'Sharp, vibrant visuals with precise pixel rendering.',
                        cinfo: '清晰、充满活力的视觉效果和精确的像素渲染。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'LED',
                        cname: '发光二极管',
                        disc: 'Brilliant, high-contrast visuals with energy-efficient light emitting diodes.',
                        cinfo: '采用节能发光二极管的明亮、高对比度视觉效果。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'OLED',
                        cname: '有机发光二极管',
                        disc: 'Deep, true black levels and vibrant, organic color reproduction.',
                        cinfo: '深，真正的黑色层次和充满活力，有机的色彩再现。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'AMOLED',
                        cname: '中文标题',
                        disc: 'Rich, vibrant visuals with deep blacks and high contrast.',
                        cinfo: '丰富，充满活力的视觉效果与深黑色和高对比度。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Plasma Display',
                        cname: '等离子显示器',
                        disc: 'Bright, vibrant visuals with wide viewing angles and rich colors.',
                        cinfo: '明亮、充满活力的视觉效果，视角宽广，色彩丰富。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Quantum Dot',
                        cname: '量子点',
                        disc: 'Vivid, lifelike visuals with enhanced color accuracy and saturation.',
                        cinfo: '生动逼真的视觉效果，增强了色彩准确性和饱和度。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Electroluminescent',
                        cname: '电致',
                        disc: 'Self-illuminating, glowing effect powered by electrical current.',
                        cinfo: '由电流供电的自发光效果。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                ],
            }, {
                tname: 'Color',
                cname: '颜色',
                children: [
                    {
                        tname: 'Amber',
                        cname: '琥珀色',
                        disc: 'Warm, golden hue resembling the color of amber gemstone.',
                        cinfo: '暖色调，类似琥珀宝石的金色。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Baby Blue',
                        cname: '粉蓝',
                        disc: 'Soft, pale blue shade reminiscent of a clear sky.',
                        cinfo: '柔和的淡蓝色阴影让人想起晴朗的天空。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Baby Pink',
                        cname: '浅粉色',
                        disc: 'Delicate, light pink hue often associated with innocence and sweetness.',
                        cinfo: '精致的淡粉色色调，常与天真和甜美联系在一起。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Beige',
                        cname: '浅褐色的',
                        disc: 'Neutral, light brown color with a hint of warmth.',
                        cinfo: '中性，浅棕色，带一丝温暖。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Blue',
                        cname: '蓝色',
                        disc: 'Cool, calming color often associated with the sky and ocean.',
                        cinfo: '凉爽、平静的颜色通常与天空和海洋联系在一起。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Brown',
                        cname: '棕色的',
                        disc: 'Earthy, warm color resembling natural tones of wood and soil.',
                        cinfo: '泥土色，暖色调，类似于木材和土壤的自然色调。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'CYMK',
                        cname: '彩色',
                        disc: 'Color model representing Cyan, Magenta, Yellow, and Key (Black) for printing.',
                        cinfo: '用于打印的代表青色、品红色、黄色和键（黑色）的颜色模型。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Citrus',
                        cname: '橘色',
                        disc: 'Bright, vibrant color reminiscent of citrus fruits like oranges and lemons.',
                        cinfo: '明亮、充满活力的颜色让人想起柑橘类水果，如橙子和柠檬。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Coquelicot',
                        cname: '中文标题',
                        disc: 'Intense, vibrant red-orange color resembling the petals of a poppy flower.',
                        cinfo: '强烈、鲜艳的红橙色，类似罂粟花的花瓣。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Cyan',
                        cname: '青色',
                        disc: 'Bright, blue-green color resembling the hues of the ocean.',
                        cinfo: '明亮的蓝绿色，类似海洋的色调。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Gold',
                        cname: '金',
                        disc: 'Rich, lustrous yellow color associated with luxury and wealth.',
                        cinfo: '丰富，有光泽的黄色与奢华和财富联系在一起。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Gray',
                        cname: '灰色',
                        disc: 'Neutral color ranging from light to dark shades of achromatic gray.',
                        cinfo: '中性色，从浅色到深色不等，为无色灰色。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Green',
                        cname: '绿色',
                        disc: 'Color of nature, symbolizing growth, freshness, and harmony.',
                        cinfo: '大自然的颜色，象征着成长、新鲜和和谐。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Hot pink',
                        cname: '鲜粉红',
                        disc: 'Bold, vibrant shade of pink with high intensity and energy.',
                        cinfo: '大胆、充满活力的粉红色色调，具有高强度和活力。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Indigo',
                        cname: '靛蓝',
                        disc: 'Deep, rich blue-purple color often associated with mystery and spirituality.',
                        cinfo: '深邃、浓郁的蓝紫色常与神秘和灵性联系在一起。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Lavender',
                        cname: '淡紫色',
                        disc: 'Soft, pale purple color resembling the delicate blooms of lavender flowers.',
                        cinfo: '柔和的淡紫色，类似淡紫色的花朵。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Magenta',
                        cname: '洋红',
                        disc: 'Bold, purplish-red color with strong vibrancy and intensity.',
                        cinfo: '大胆的紫红色，具有强烈的活力和强度。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Matte Black',
                        cname: '哑光黑色',
                        disc: 'Deep, velvety black color with a non-reflective, flat finish.',
                        cinfo: '深如天鹅绒般的黑色，表面无反光，平整。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Mint',
                        cname: '造币厂',
                        disc: 'Cool, refreshing light green color resembling the leaves of mint plants.',
                        cinfo: '凉爽清爽的浅绿色，类似薄荷植物的叶子。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Navy Blue',
                        cname: '海军蓝',
                        disc: 'Dark, deep shade of blue resembling the color of the night sky.',
                        cinfo: '一种深蓝色，类似于夜空的颜色。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Neon Blue',
                        cname: '霓虹蓝色',
                        disc: 'Intensely vibrant, electric shade of blue that catches attention.',
                        cinfo: '强烈的活力，蓝色的电色调，引人注目。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Neon Green',
                        cname: '荧光绿色',
                        disc: 'Intensely bright and fluorescent shade of green that stands out.',
                        cinfo: '强烈明亮的荧光色调的绿色脱颖而出。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    }
                ]
            }, {
                tname: 'Color More',
                cname: '更多颜色',
                children: [
                    {
                        tname: 'Neon Orange',
                        cname: '荧光橙色',
                        disc: 'Intensely vibrant, fluorescent shade of orange that demands attention.',
                        cinfo: '强烈的活力，荧光橙色阴影，需要注意。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Neon Purple',
                        cname: '霓虹紫色',
                        disc: 'Intensely bright and vibrant shade of purple with a fluorescent quality.',
                        cinfo: '强烈明亮和充满活力的紫色阴影，具有荧光品质。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Neon Red',
                        cname: '霓虹红',
                        disc: 'Intensely bright and vibrant shade of red with a fluorescent quality.',
                        cinfo: '强烈明亮和充满活力的红色阴影，具有荧光品质。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Neon Yellow',
                        cname: '荧光黄色',
                        disc: 'Intensely bright and fluorescent shade of yellow that catches the eye.',
                        cinfo: '强烈明亮的荧光黄色阴影，引人注目。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Orange',
                        cname: '橙色',
                        disc: 'Warm, vibrant color combining the energy of red and the brightness of yellow.',
                        cinfo: '温暖、充满活力的色彩融合了红色的活力和黄色的明亮。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Pastel',
                        cname: '粉彩',
                        disc: 'Soft, muted color with a gentle and light appearance.',
                        cinfo: '柔和柔和的颜色，外观柔和轻盈。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Pink',
                        cname: '粉红色',
                        disc: 'Delicate, light color often associated with femininity and sweetness.',
                        cinfo: '精致的浅色通常与女性气质和甜美联系在一起。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'RGB',
                        cname: '三原色',
                        disc: 'Color model combining Red, Green, and Blue to create a wide range of colors.',
                        cinfo: '颜色模型将红色、绿色和蓝色相结合，以创建各种颜色。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Red',
                        cname: '红色',
                        disc: 'Intense, passionate color associated with energy, love, and power.',
                        cinfo: '与能量、爱和力量相关的强烈、激情的色彩。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Silver',
                        cname: '银',
                        disc: 'Shiny, metallic color resembling the hue of silver metal.',
                        cinfo: '闪亮的金属色，类似于银色金属的色调。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Teal',
                        cname: '青色',
                        disc: 'Blue-green color reminiscent of the hues found in tropical waters.',
                        cinfo: '蓝绿色让人想起热带水域的色调。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Turquoise',
                        cname: '绿松石',
                        disc: 'Vibrant, blue-green color resembling the gemstone of the same name.',
                        cinfo: '鲜艳的蓝绿色，类似于同名宝石。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Vermillion',
                        cname: '朱红色',
                        disc: 'Rich, vivid shade of red-orange resembling the hue of cinnabar.',
                        cinfo: '浓郁、生动的红橙色，类似朱砂的色调。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'White',
                        cname: '白色',
                        disc: 'Pure, pristine color representing lightness, purity, and clarity.',
                        cinfo: '纯净、质朴的颜色，代表着轻盈、纯净和清澈。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Yellow',
                        cname: '黄色',
                        disc: 'Bright, cheerful color associated with sunshine, happiness, and optimism.',
                        cinfo: '明亮、欢快的色彩与阳光、幸福和乐观联系在一起。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },

                ]
            }
        ]
    },
    //第一类目第二项Camera&View
    {
        tname: 'Camera&View',
        cname: '相机和场景',
        children: [
            {
                tname: "Lenses&Perspective",
                cname: '镜头和透视',
                children: [
                    {
                        tname: 'Macro',
                        cname: '宏',
                        disc: 'Photography lens designed for close-up shots with high magnification.',
                        cinfo: '专为高倍率特写镜头设计的摄影镜头。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Microscopic',
                        cname: '微观',
                        disc: 'A style that simulates the look of an image captured under a microscope,with a focus on the microscopic details of the image. This can create an abstract and surreal effect, revealing patterns and structures that are invisible to the naked eye.',
                        cinfo: '一种风格，模拟在显微镜下拍摄的图像的外观，重点关注图像的微观细节。这可以创造一种抽象和超现实的效果，揭示肉眼看不见的图案和结构。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Super-Resolution Microscopic',
                        cname: '超分辨率显微镜',
                        disc: 'Advanced imaging technique providing higher resolution and detailed visualization at the microscopic level.',
                        cinfo: '先进的成像技术，在微观层面提供更高的分辨率和详细的可视化。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Wide-Angle',
                        cname: '广角',
                        disc: 'Lens or photography technique capturing a broad field of view, expanding the perspective.',
                        cinfo: '镜头或摄影技术捕捉广阔的视野，扩大视角。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Ultra-wide Angle',
                        cname: '超广角',
                        disc: 'Extremely wide lens or technique capturing an even broader field of view, expanding the perspective to an exceptional extent.',
                        cinfo: '非常宽的镜头或技术可以捕捉更宽的视野，将视角扩展到一个特殊的程度。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Panorama',
                        cname: '全景',
                        disc: 'A style that captures a 360-degree image of the scene, with a wide and expansive view that can create a sense of immersion and grandeur.',
                        cinfo: '一种捕捉360度场景图像的风格，视野开阔，可以营造出一种沉浸感和宏伟感。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: '360 Panorama',
                        cname: '360全景',
                        disc: 'Technique capturing a complete view in every direction, creating an immersive, panoramic image or video.',
                        cinfo: '捕捉各个方向的完整视图的技术，创建身临其境的全景图像或视频。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Fisheye Lens',
                        cname: '鱼眼透镜',
                        disc: 'Specialty lens creating a wide-angle, distorted perspective with a curved appearance.',
                        cinfo: '特殊镜头，创造了一个广角、扭曲的视角和弯曲的外观。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Aerial View',
                        cname: '鸟瞰图',
                        disc: "A camera angle that captures the scene from above, with a focus on the subject's position in the larger environment. This can create a sense of scale and perspective, with a focus on the subject's relationship to the wider world.",
                        cinfo: '一种从上方捕捉场景的相机角度，聚焦于主体在更大环境中的位置。这可以创造一种尺度感和视角感，重点关注主体与更广阔世界的关系。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Top View',
                        cname: '俯视图',
                        disc: "Perspective capturing the scene or object from directly above, providing a bird's-eye view.",
                        cinfo: '从正上方捕捉场景或对象的透视图，提供鸟瞰图。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Side View',
                        cname: '侧视图',
                        disc: 'Perspective capturing the scene or object from a lateral angle, showing the profile or side profile.',
                        cinfo: '从横向角度捕捉场景或对象的透视图，显示轮廓或侧面轮廓。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Satellite View',
                        cname: '卫星视图',
                        disc: "Aerial perspective captured by satellite imagery, providing a bird's-eye view of Earth's surface.",
                        cinfo: '卫星图像捕捉到的空中视角，提供了地球表面的鸟瞰图。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Product View',
                        cname: '产品视图',
                        disc: 'Visual presentation showcasing the features, details, and appearance of a specific product.',
                        cinfo: '展示特定产品的功能、细节和外观的视觉演示。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Closeup',
                        cname: '特写镜头',
                        disc: "A camera angle that captures the subject's face or body in close proximity, with a focus on detail and texture. This can create a sense of intensity or intimacy, with a focus on the subject's physical presence.",
                        cinfo: '展示特定产品的功能、细节和外观的视觉演示。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'First-Person',
                        cname: '第一人称',
                        disc: "Perspective capturing the scene or experience from the viewpoint of the person involved, providing a subjective and immersive view.",
                        cinfo: '从相关人员的角度捕捉场景或体验，提供主观和身临其境的视角。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Third-Person',
                        cname: '第三人称',
                        disc: "Perspective capturing the scene or experience from an external viewpoint, observing the subject from a detached or objective standpoint.",
                        cinfo: '从外部视角捕捉场景或体验，从独立或客观的角度观察主题。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Front View',
                        cname: '中文标题',
                        disc: "A camera angle that captures the scene from the front of the subject, with a focus on the subject's facial expressions and emotions. This can create a sense of intimacy or confrontation, with a focus on the subject's external reactions.",
                        cinfo: '从拍摄对象的正面捕捉场景的相机角度，重点关注拍摄对象的面部表情和情绪。这可以产生一种亲密感或对抗感，重点关注受试者的外部反应。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Rear View',
                        cname: '前视图',
                        disc: "A camera angle that captures the scene from behind the subject, with a focus on the subject's posture and body language. This can create a sense of mystery or detachment, with a focus on the subject's hidden or internal thoughts.",
                        cinfo: '从拍摄对象背后捕捉场景的相机角度，重点关注拍摄对象的姿势和肢体语言。这可以创造一种神秘感或超然感，关注主体隐藏或内心的想法。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                ]
            }, {
                tname: "Scenes",
                cname: '场景',
                children: [
                    {
                        tname: 'film',
                        cname: '电影',
                        disc: 'Sequential segments within a movie that depict specific events, actions, or dialogues to advance the story or convey emotions.',
                        cinfo: '电影中描述特定事件、动作或对话以推进故事或传达情感的连续片段。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'photography',
                        cname: '照相术',
                        disc: 'Compositionally arranged settings or subjects captured through photography, aiming to convey a particular mood, story, or aesthetic.',
                        cinfo: '通过摄影捕捉的构图安排的场景或主题，旨在传达特定的情绪、故事或美学。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'time-lapse',
                        cname: '延时拍摄的',
                        disc: 'A style that captures the scene over an extended period of time, using time-lapse technology to condense hours or days into a single image. This can create a surreal and dreamlike effect, with a sense of time and motion.',
                        cinfo: '一种长时间捕捉场景的风格，使用延时技术将数小时或数天浓缩成一张图像。这可以创造一种超现实和梦幻般的效果，具有时间感和运动感。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'portrait',
                        cname: '肖像',
                        disc: 'A photograph or painting capturing the likeness and essence of a person, typically focusing on their face and expressions.',
                        cinfo: '一张照片或一幅画，捕捉一个人的肖像和本质，通常集中在他们的脸和表情上。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'bokeh',
                        cname: '散景',
                        disc: 'Visual effect created by the out-of-focus areas in a photograph, characterized by soft, blurred circles or shapes, often achieved with a wide aperture.',
                        cinfo: '由照片中的失焦区域产生的视觉效果，其特征是柔和、模糊的圆圈或形状，通常用大光圈实现。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'cinematic',
                        cname: '电影的',
                        disc: 'Pertaining to the characteristics and style of film, evoking a visual and immersive experience similar to that of movies.',
                        cinfo: '用于修饰或说明电影的特点和风格，唤起类似于电影的视觉和身临其境的体验。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'highspeed',
                        cname: '高速',
                        disc: 'A style that captures the scene at a high frame rate, using high-speed technology to slow down and capture fast-moving action. This can create a dramatic and intense effect, with a focus on the interplay of motion and time.',
                        cinfo: '一种以高帧率捕捉场景的风格，使用高速技术来减慢速度并捕捉快速移动的动作。这可以创造一个戏剧性和强烈的效果，重点是运动和时间的相互作用。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Stereoscopic',
                        cname: '立体',
                        disc: 'A style that uses stereoscopic imaging to create a 3D effect, with a focus on depth and dimension. This can create a realistic and immersive effect, with a sense of spatial awareness and presence.',
                        cinfo: '一种使用立体成像来创建3D效果的风格，重点关注深度和维度。这可以创造一种逼真和身临其境的效果，具有空间意识和存在感。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: '360 VR',
                        cname: '360虚拟现实',
                        disc: 'A style that captures a 360-degree virtual reality image of the scene, with a focus on immersion and interactivity. This can create a sense of presence and spatial awareness, with a focus on interactivity and exploration.',
                        cinfo: '一种捕捉场景360度虚拟现实图像的风格，注重沉浸感和互动性。这可以创造一种存在感和空间意识，重点是互动和探索。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },

                    {
                        tname: 'LIDAR',
                        cname: '激光雷达',
                        disc: 'A style that uses LIDAR technology to capture a 3D map of the scene, with a focus on depth and geometry. This can create an abstract and futuristic effect, with a sense of dimension and spatial awareness.',
                        cinfo: '一种使用激光雷达技术捕捉场景三维地图的风格，重点关注深度和几何图形。这可以创造一种抽象和未来的效果，具有维度感和空间意识。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'HDR',
                        cname: '半双工转发器',
                        disc: 'A style that combines multiple exposures of the same scene to create an image with a high dynamic range, with details in both the highlights and shadows. This can create a realistic and immersive effect, with a sense of depth and detail.',
                        cinfo: '一种将同一场景的多次曝光组合在一起以创建具有高动态范围的图像的样式，其中高光和阴影中都有细节。这可以创造一种逼真和身临其境的效果，具有深度感和细节感。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },

                ]
            }, {
                tname: "Type",
                cname: '类型',
                children: [
                    {
                        tname: '35mm',
                        cname: '35毫米',
                        disc: 'Retro lens flare frames a timeless moment.',
                        cinfo: '复古的闪光镜头勾勒出永恒的瞬间。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'DSLR',
                        cname: '数码单反相机',
                        disc: 'Crisp focus captures vivid details in high definition.',
                        cinfo: '清晰的焦点捕捉清晰的细节。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: '70mm',
                        cname: '70毫米',
                        disc: 'Expansive depth unveils grandeur with cinematic precision.',
                        cinfo: '广阔的深度以电影般的精准展现出宏伟。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Intax',
                        cname: 'Intax公司',
                        disc: 'Soft, dreamlike hues blend instant memories with nostalgia.',
                        cinfo: '柔和、梦幻般的色调将即时记忆与怀旧交织在一起。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'DJI Mavic Air 2',
                        cname: '中文标题',
                        disc: 'Drone with high-resolution camera for aerial photography.',
                        cinfo: '无人机与高分辨率相机用于航空摄影。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Leica M10',
                        cname: '徕卡M10',
                        disc: 'Perfect for street photography with excellent low-light capabilities.',
                        cinfo: '非常适合街头摄影，具有出色的微光功能。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Nikon D850',
                        cname: '尼康D850',
                        disc: 'Ideal for capturing wildlife in motion.',
                        cinfo: '非常适合捕捉运动中的野生动物。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Sony a7 III',
                        cname: '索尼a7 III',
                        disc: 'Great for landscape photography with wide angles and excellent clarity.',
                        cinfo: '非常适合广角和出色清晰度的风景摄影。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: ' Fujifilm X-T4',
                        cname: '富士胶片X-T4',
                        disc: 'Capturing stunning portraits with beautiful bokeh.',
                        cinfo: '用美丽的散焦捕捉令人惊叹的肖像。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Konica Hexar AF',
                        cname: '柯尼卡Hexar AF',
                        disc: '35mm film camera with excellent autofocus and lens. ',
                        cinfo: '35毫米胶片相机，具有出色的自动对焦和镜头。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    }
                ]
            }, {
                tname: "Type More",
                cname: '更多类型',
                children: [
                    {
                        tname: 'Arri Alexa Mini LF',
                        cname: '中文标题',
                        disc: 'Cinema camera with high-quality lenses for professional use. ',
                        cinfo: '配备高品质专业镜头的影院摄像机。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'RED Monstro 8K VV',
                        cname: '中文标题',
                        disc: 'High-end cinema camera with advanced features. ',
                        cinfo: '具有先进功能的高端影院摄像机。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Blackmagic URSA Mini Pro 12K',
                        cname: '中文标题',
                        disc: 'Professional cinema camera with advanced technology.',
                        cinfo: '采用先进技术的专业影院摄像机。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Panasonic GH5',
                        cname: '松下GH5',
                        disc: 'Mirrorless camera with high-quality lens for video and photo.',
                        cinfo: '无镜相机，带高质量镜头，可拍摄视频和照片。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Olympus Tough TG-6',
                        cname: '奥林巴斯坚韧TG-6',
                        disc: 'Waterproof camera for outdoor photography and adventures.',
                        cinfo: '防水相机，适合户外摄影和探险。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Ricoh GR III',
                        cname: '理光GR III',
                        disc: 'Compact camera for street and travel photography.',
                        cinfo: '小型相机，用于街头和旅行摄影。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Leica Q2',
                        cname: '徕卡Q2',
                        disc: 'Full-frame compact camera with high-quality lens.',
                        cinfo: '全画幅紧凑型相机，配高品质镜头。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Polaroid Now',
                        cname: '宝丽来现在',
                        disc: 'Instant camera with a classic, vintage style.',
                        cinfo: '具有经典复古风格的即时照相机。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Lomography Diana F+',
                        cname: '中文标题',
                        disc: 'Film camera with a unique and creative look.',
                        cinfo: '具有独特创意外观的胶片相机。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Hasselblad 500CM',
                        cname: '哈苏500CM',
                        disc: 'Medium format film camera with high-quality lens for portraits.',
                        cinfo: '中画幅胶片相机，配有高质量的人像镜头。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Polaroid SX-70',
                        cname: '宝丽来SX-70',
                        disc: 'Classic instant film camera with a vintage look.',
                        cinfo: '复古外观的经典即时胶片相机。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Yashica Mat-124G',
                        cname: '亚希卡Mat-124G',
                        disc: 'Medium format TLR camera with advanced features.',
                        cinfo: '具有高级功能的中格式TLR相机。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Hasselblad X2D 100C',
                        cname: '哈苏X2D 100C',
                        disc: 'High-resolution images with advanced features.',
                        cinfo: '具有高级功能的高分辨率图像。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Sony A7S III',
                        cname: '索尼A7S III',
                        disc: 'Mirrorless camera with excellent low-light capabilities.',
                        cinfo: '无镜相机具有出色的微光功能。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Minolta SRT-101',
                        cname: '美能达SRT-101',
                        disc: 'SLR camera with high-quality lenses for film photography.',
                        cinfo: '单反相机，配有高质量镜头，用于胶片摄影。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },

                ]
            }

        ]
    },
    //第一类目第三项Style&Effect
    {
        tname: "Style&Effect",
        cname: '样式和效果',
        children: [
            {
                tname: 'Artist',
                cname: '艺术',
                children: [
                    {
                        tname: 'Alphonse Mucha',
                        cname: '中文标题',
                        disc: "Art Nouveau illustrations with intricate floral designs.",
                        cinfo: '新艺术运动插图与复杂的花卉设计。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Andy Warhol',
                        cname: '安迪·沃霍尔',
                        disc: "Pop art paintings and silkscreens of celebrity icons.",
                        cinfo: '流行艺术绘画和名人标志的丝网印刷。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Art by Yoko Ono',
                        cname: '小野洋子艺术',
                        disc: "Conceptual and performance art, often with political messages",
                        cinfo: '概念艺术和行为艺术，通常带有政治信息',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Banksy',
                        cname: '班克西',
                        disc: "Street art with political and social commentary.",
                        cinfo: '带有政治和社会评论的街头艺术。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'By Francisco de Goya',
                        cname: '弗朗西斯科·德·戈雅',
                        disc: "Baroque and Romantic paintings with dark, political themes.",
                        cinfo: '带有黑暗政治主题的巴洛克和浪漫主义绘画。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Caravaggio',
                        cname: '卡拉瓦乔',
                        disc: "Baroque paintings with dramatic lighting and realistic figures.",
                        cinfo: '具有戏剧性的灯光和逼真人物的巴洛克绘画。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'David Hockney',
                        cname: '大卫·霍克尼',
                        disc: "Bright and colorful paintings with modernist perspectives.",
                        cinfo: '明亮多彩的绘画，具有现代主义的视角。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Diego Rivera',
                        cname: '里维拉',
                        disc: "Muralist with social and political themes in his work.",
                        cinfo: '作品中带有社会和政治主题的壁画家。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Edgar Degas',
                        cname: '埃德加·德加',
                        disc: "Impressionist paintings of dancers and Parisian life.",
                        cinfo: '关于舞者和巴黎生活的印象派绘画。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Eugene Delacroix',
                        cname: '德拉克洛瓦',
                        disc: "Romantic paintings with vibrant colors and exotic themes.",
                        cinfo: '充满活力的色彩和异国情调的主题的浪漫主义绘画。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Francis Bacon',
                        cname: '弗朗西斯·培根',
                        disc: "Expressionist paintings with distorted figures and dark emotions.",
                        cinfo: '弗朗西斯·巴康具有扭曲的人物和黑暗情感的表现主义绘画。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Frida Kahlo',
                        cname: '弗里达·卡罗',
                        disc: "Surrealist self-portraits with personal and political themes.",
                        cinfo: '具有个人和政治主题的超现实主义自画像。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Garald Brom',
                        cname: '加拉尔德布罗姆',
                        disc: "Dark fantasy and horror illustrations with detailed textures.",
                        cinfo: '带有详细纹理的黑暗幻想和恐怖插图。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Gustav Klimt',
                        cname: '古斯塔夫·克里姆特',
                        disc: "Art Nouveau paintings with decorative and sensual details.",
                        cinfo: '具有装饰性和感性细节的新艺术绘画。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Henri Matisse',
                        cname: '亨利·马蒂斯',
                        disc: "Fauvist paintings with bold colors and simplified forms.",
                        cinfo: '大胆色彩和简化形式的野兽派绘画。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'JMW Turner',
                        cname: '特纳',
                        disc: "Romantic landscape paintings with expressive brushstrokes.",
                        cinfo: '浪漫的风景画，富有表现力的笔触。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Jack Kirby',
                        cname: '捷克科比',
                        disc: "Comic book art, co-creator of Marvel's most iconic characters.",
                        cinfo: '漫画艺术，漫威最具标志性人物的联合创作者。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Jackson Pollock',
                        cname: '波洛克',
                        disc: "Abstract expressionist paintings with dripped and poured paint.",
                        cinfo: '杰克逊·波洛克抽象表现主义绘画，用滴落和倾倒的颜料。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Jean Michel Basquiat',
                        cname: '吉恩-米歇尔·巴斯奎特',
                        disc: "Graffiti-style art with social commentary and bold symbolism.",
                        cinfo: '具有社会评论和大胆象征意义的涂鸦风格艺术。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },

                ]
            },
            {
                tname: 'Artist More',
                cname: '更多艺术',
                children: [
                    {
                        tname: 'Johannes Vermeer',
                        cname: '约翰内斯·维米尔',
                        disc: "Baroque paintings with meticulous detail and luminous light.",
                        cinfo: '细节细致、光线明亮的巴洛克绘画。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Leonardo Da Vinci',
                        cname: '列奥纳多·达·芬奇',
                        disc: "Renaissance paintings and inventions with scientific accuracy.",
                        cinfo: '文艺复兴时期具有科学准确性的绘画和发明。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Marc Chagall',
                        cname: '马克·夏加尔',
                        disc: "Surrealist paintings with dream-like imagery and Jewish themes.",
                        cinfo: '超现实主义绘画，具有梦幻般的意象和犹太主题。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Marcel Duchamp',
                        cname: '马塞尔·杜尚',
                        disc: "Marcel Duchamp: Dadaist sculptures and conceptual art.",
                        cinfo: '马塞尔·杜尚：达达主义雕塑和概念艺术。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Mark Rothko',
                        cname: '马克·罗斯科',
                        disc: "Abstract expressionist paintings with fields of color.",
                        cinfo: '抽象表现主义绘画与色彩领域。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Michelangelo',
                        cname: '米开朗基罗',
                        disc: "Renaissance sculptures and paintings with classical motifs.",
                        cinfo: '文艺复兴时期带有古典主题的雕塑和绘画。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Monet',
                        cname: '莫奈',
                        disc: "Impressionist paintings with loose brushwork and atmospheric light.",
                        cinfo: '印象派绘画，笔触松散，光线大气。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Paul Cezanne',
                        cname: '保罗·塞尚',
                        disc: "Post-Impressionist paintings with geometric forms and brushstrokes.",
                        cinfo: '后印象派绘画中的几何形式和笔触。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Paul Gauguin',
                        cname: '保罗·高更',
                        disc: "Post-Impressionist paintings with exotic and primitive themes.",
                        cinfo: '具有异国情调和原始主题的后印象派绘画。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Paul Klee',
                        cname: '克利',
                        disc: "Expressionist paintings with childlike simplicity and whimsy.",
                        cinfo: '具有童真质朴和奇思妙想的表现主义绘画。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Picasso',
                        cname: '毕加索',
                        disc: "Cubist paintings with fragmented and abstracted forms.",
                        cinfo: '立体派绘画，具有零散和抽象的形式。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Pierre Auguste Renoir',
                        cname: '雷诺阿',
                        disc: "Pierre Auguste Renoir: Impressionist paintings with light and airy colors.",
                        cinfo: '皮埃尔·奥古斯特·雷诺阿：印象派绘画，色彩轻快。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Piet Mondrian',
                        cname: '蒙德里安',
                        disc: "Abstract paintings with geometric shapes and primary colors.",
                        cinfo: '具有几何形状和原色的抽象绘画。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Rembrandt',
                        cname: '伦勃朗',
                        disc: "Baroque paintings with dramatic lighting and realistic figures.",
                        cinfo: '具有戏剧性的灯光和逼真人物的巴洛克绘画。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Rene Magritte',
                        cname: '马格里特',
                        disc: "Surrealist paintings with unexpected juxtapositions and absurdity.",
                        cinfo: '超现实主义绘画中出人意料的并置和荒诞。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Roy Lichtenstein',
                        cname: '罗伊·利希滕斯坦',
                        disc: "Pop art paintings with comic book style and bold lines.",
                        cinfo: '具有漫画风格和大胆线条的波普艺术绘画。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Salvador Dali',
                        cname: '萨尔瓦多·达利',
                        disc: "Surrealist paintings with dream-like and bizarre imagery.",
                        cinfo: '具有梦幻般奇异意象的超现实主义绘画。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Sandro Botticelli',
                        cname: '桑德罗·波提切利',
                        disc: "Renaissance paintings with classical and mythological themes.",
                        cinfo: '文艺复兴时期以古典和神话为主题的绘画。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Takashi Murakami',
                        cname: '村上隆',
                        disc: "Contemporary art with pop culture and anime influences.",
                        cinfo: '受流行文化和动漫影响的当代艺术。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Van Gogh',
                        cname: '梵高',
                        disc: "Post-Impressionist paintings with thick brushstrokes and intense colors.",
                        cinfo: '后印象派绘画，笔触厚重，色彩浓烈。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Wassily Kandinsky',
                        cname: '瓦西里·康定斯基',
                        disc: "Abstract paintings with vibrant colors and geometric shapes.",
                        cinfo: '抽象的绘画，色彩鲜艳，几何形状优美。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Willem De Kooning',
                        cname: '威廉·德·库宁',
                        disc: "Abstract expressionist paintings with gestural brushstrokes.",
                        cinfo: '抽象表现主义绘画与手势笔触。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Yayoi Kusama',
                        cname: '草间弥生',
                        disc: "Contemporary art with repeating patterns and dots.",
                        cinfo: '具有重复图案和圆点的当代艺术。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Yoji Shinkawa',
                        cname: '新川洋司',
                        disc: "Video game concept art with intricate designs and dramatic action.",
                        cinfo: '具有复杂设计和戏剧性动作的电子游戏概念艺术。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                ]
            },
            {
                tname: 'Theme',
                cname: '主题',
                children: [
                    {
                        tname: '1800s',
                        cname: '19世纪',
                        disc: "Artistic style from the 19th century.",
                        cinfo: '19世纪的艺术风格。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                ]
            },
            {
                tname: 'Medium',
                cname: '材质',
                children: [
                    {
                        tname: 'Mosaic',
                        cname: '马赛克',
                        disc: "Art made by assembling small pieces of colored glass, stone, or tile.",
                        cinfo: '由小块彩色玻璃、石头或瓷砖组装而成的艺术。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Collage',
                        cname: '拼贴画',
                        disc: "Art made by gluing together pieces of paper, fabric, or other materials.",
                        cinfo: '将纸张、织物或其他材料粘合在一起的艺术。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Tapestry',
                        cname: '壁毯',
                        disc: "Woven textile art made on a loom with designs or images.",
                        cinfo: '在织机上制作的带有图案或图像的纺织艺术。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Etching',
                        cname: '蚀刻',
                        disc: "Intaglio printmaking technique using acid to create lines in a metal plate.",
                        cinfo: '凹版印刷技术使用酸在金属板上创造线条。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Charcoal drawing',
                        cname: '木炭画',
                        disc: "Art made with charcoal, often used for sketches and portraits.",
                        cinfo: '用木炭做成的艺术，常用于素描和肖像画。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Watercolor',
                        cname: '水彩',
                        disc: "Painting with water-based pigments on paper.",
                        cinfo: '用水性颜料在纸上作画。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Wood carving',
                        cname: '木雕',
                        disc: "Sculpture made by carving wood with tools.",
                        cinfo: '用工具雕刻木头制成的雕塑。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Glassblowing',
                        cname: '玻璃吹制',
                        disc: "Art of forming molten glass by blowing air into a tube.",
                        cinfo: '将空气吹入管中形成熔融玻璃的艺术。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Calligraphy',
                        cname: '书法',
                        disc: "Decorative handwriting with a pen or brush.",
                        cinfo: '用钢笔或刷子写的装饰性笔迹。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Chalk pastel drawing',
                        cname: '粉笔画',
                        disc: "Art made with soft pastel chalks on paper.",
                        cinfo: '用柔软的粉彩粉笔在纸上做成的艺术品。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Digital art',
                        cname: '数字艺术',
                        disc: "Art made using digital technology such as computer programs.",
                        cinfo: '使用计算机程序等数字技术制作的艺术品。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Graffiti',
                        cname: '涂鸦',
                        disc: "Art made by writing or drawing on public surfaces.",
                        cinfo: '在公共场所书写或绘画的艺术。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Performance art',
                        cname: '行为艺术',
                        disc: "Live art performance, often incorporating theater or dance.",
                        cinfo: '现场艺术表演，通常包括戏剧或舞蹈。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Metalwork',
                        cname: '金属制品',
                        disc: "Art made by forging, casting, or welding metal.",
                        cinfo: '通过锻造、铸造或焊接金属制成的艺术。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Embroidery',
                        cname: '湘绣',
                        disc: "Decorative stitching on fabric with thread or yarn.",
                        cinfo: '用线或纱线在织物上进行装饰性缝合。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Pottery',
                        cname: '陶器',
                        disc: "Art of making objects from clay, fired at high temperatures.",
                        cinfo: '用粘土在高温下烧制物品的艺术。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Engraving',
                        cname: '雕刻',
                        disc: "Printmaking technique using incised lines on a metal plate.",
                        cinfo: '利用金属板上的刻线进行印刷的技术。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Printmaking',
                        cname: '版画制作',
                        disc: "Art of creating multiple prints from a single design.",
                        cinfo: '从一个设计中创造多个版画的艺术。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Origami',
                        cname: '折纸',
                        disc: "Art of folding paper into decorative shapes or figures.",
                        cinfo: '把纸折成装饰性的形状或图形的艺术。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Sand art',
                        cname: '沙子艺术',
                        disc: "Art made by arranging sand into patterns or designs.",
                        cinfo: '把沙子排列成图案或图案的艺术。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: '3D printing',
                        cname: '3D打印',
                        disc: "Art made using a 3D printer, which prints three-dimensional objects.",
                        cinfo: '使用打印三维物体的3D打印机制作的艺术品。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Paper mache',
                        cname: '混凝纸浆',
                        disc: "Art made by shaping and sticking layers of paper and adhesive.",
                        cinfo: '通过将纸和粘合剂层成型并粘贴而成的艺术。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Beadwork',
                        cname: '串珠',
                        disc: "Art made by stringing beads together into patterns or designs.",
                        cinfo: '把珠子串在一起做成图案或图案的艺术。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Leatherwork',
                        cname: '皮革制品',
                        disc: "Art of crafting objects from leather, often used for belts or bags.",
                        cinfo: '用皮革制作物品的艺术，通常用于皮带或袋子。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Mixed media art',
                        cname: '混合媒体艺术',
                        disc: "Art made by combining different materials or techniques.",
                        cinfo: '由不同材料或技术组合而成的艺术。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                ]
            }, {
                tname: 'Mood',
                cname: '氛围',
                children: [
                    {
                        tname: 'Joyful',
                        cname: '快乐',
                        disc: "Artistic style from the 19th century.",
                        cinfo: '19世纪的艺术风格。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Serene',
                        cname: '平静的',
                        disc: "Gentle ripples calm the soul in a tranquil oasis.",
                        cinfo: '在宁静的绿洲中，轻柔的涟漪使灵魂平静下来。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Playful',
                        cname: '好玩的',
                        disc: "Whimsical splashes of color ignite a joyful wonderland.",
                        cinfo: '飞溅的色彩点燃了一片欢乐的仙境。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Mysterious',
                        cname: '神秘的',
                        disc: "Ethereal shadows whisper secrets in enigmatic darkness.",
                        cinfo: '以太的影子在神秘的黑暗中窃窃私语。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Melancholy',
                        cname: '悲哀的',
                        disc: "Subtle shades of gray evoke a bittersweet, introspective mood.",
                        cinfo: '微妙的灰色色调唤起一种苦乐参半、内省的心情。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Powerful',
                        cname: '强大的',
                        disc: "Explosions of energy unleash a formidable force.",
                        cinfo: '能量的爆发释放出一股强大的力量。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Nostalgic',
                        cname: '怀旧',
                        disc: "Soft filters transport you to a cherished bygone era.",
                        cinfo: '软过滤器将您带到一个珍贵的过去时代。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Intense',
                        cname: '强烈的',
                        disc: "Fiery sparks ignite a torrent of electrifying intensity.",
                        cinfo: '炽热的火花点燃了一股激动人心的洪流。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Romantic',
                        cname: '浪漫的',
                        disc: "Gentle candlelight illuminates a tender, love-filled atmosphere.",
                        cinfo: '柔和的烛光照亮了温柔的、充满爱的气氛。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Hopeful',
                        cname: '充满希望的',
                        disc: "Glowing rays of light pierce through clouds, inspiring optimism.",
                        cinfo: '灿烂的光线穿透云层，激发乐观情绪。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Energetic',
                        cname: '精力充沛的',
                        disc: "Dynamic lines pulsate with vibrant energy and unstoppable motion.",
                        cinfo: '动感线条充满活力，运动势不可挡。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Tranquil',
                        cname: '安逸',
                        disc: "Soft, serene waves lull you into a peaceful state of tranquility.",
                        cinfo: '柔和、宁静的海浪会让你进入平静的状态。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Contemplative',
                        cname: '沉思的',
                        disc: "Subtle, muted tones evoke deep introspection and thoughtful reflection.",
                        cinfo: '微妙、柔和的音调唤起深刻的反省和深思熟虑的思考。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Dreamy',
                        cname: '梦幻般的',
                        disc: "Soft focus and pastel hues create a whimsical, ethereal ambiance.",
                        cinfo: '柔和的焦点和柔和的色调营造出一种异想天开、空灵的氛围。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Dramatic',
                        cname: '戏剧性的',
                        disc: "Bold contrasts and intense shadows heighten the theatrical impact.",
                        cinfo: '大胆的对比和强烈的阴影增强了戏剧效果。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Whimsical',
                        cname: '异想天开的',
                        disc: "Playful swirls and vibrant colors create a whimsical fantasy world.",
                        cinfo: '有趣的漩涡和鲜艳的色彩创造了一个异想天开的幻想世界。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Pensive',
                        cname: '沉思',
                        disc: "Subdued tones and soft lighting evoke a reflective, introspective mood.",
                        cinfo: '柔和的色调和柔和的灯光唤起了一种反思和内省的情绪。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Optimistic',
                        cname: '乐观的',
                        disc: "Radiant sunlight illuminates a hopeful, bright outlook on life.",
                        cinfo: '灿烂的阳光照亮了充满希望、光明的人生前景。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Adventurous',
                        cname: '冒险',
                        disc: "Dynamic angles and wide vistas ignite a sense of daring exploration.",
                        cinfo: '动态的角度和广阔的视野点燃了一种大胆探索的感觉。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Enigmatic',
                        cname: '神秘的',
                        disc: "Mysterious shadows and intriguing patterns invite curious contemplation.",
                        cinfo: '神秘的阴影和迷人的图案吸引着好奇的沉思。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                ]
            }
        ]
    },
    //第一类目第四项Model&Parameters
    {
        tname: "Model&Parameters",
        cname: '模型和参数',
        children: [
            {
                tname: 'Parameter',
                cname: '参数',
                children: [
                    {
                        tname: 'aspect ratio',
                        cname: '纵横比',
                        disc: "Frame shape that defines visual composition.",
                        cinfo: '定义视觉合成的框架形状。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Image weight',
                        cname: '图像权重',
                        disc: "Sets image prompt weight relative to text weight. The default value is --iw 0.25.",
                        cinfo: '设置图像提示相对于文本权重的权重。默认值为--iw 0.25。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'No',
                        cname: '空',
                        disc: "Negative prompting, --no plants would try to remove plants from the image.",
                        cinfo: '负面提示——没有植物会试图从图像中删除植物。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'quality',
                        cname: '质量',
                        disc: "lets you give the algorithm more or less time to think. It also changes the cost of your images. Supported values: 0.25, 0.5, 1, 2, and 5.",
                        cinfo: '让您给算法或多或少的思考时间。它还改变了图像的成本。支持的值：0.25、0.5、1、2和5。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'stop',
                        cname: '停止',
                        disc: "stop the generation procss.",
                        cinfo: '停止生成进程。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'stylize',
                        cname: '使风格化',
                        disc: "625 turns it off, 1250 is recommended for experienced users, 2500 is the default, while 20000+ means very strong stylization.",
                        cinfo: '625将其关闭，1250推荐给有经验的用户，2500是默认值，而20000+意味着非常强烈的风格化。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'chaos',
                        cname: '混乱',
                        disc: "the level of abstraction. 0 is the default meaning a very literal prompt, while 100 is the maximum, to let chaos reign supreme.",
                        cinfo: '抽象的层次。0是默认值，意味着一个非常字面的提示，而100是最大值，让混乱占据主导地位。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Seed',
                        cname: '种子',
                        disc: "to ensure consistent look and feel",
                        cinfo: '以确保外观和感觉一致',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Tile',
                        cname: '磁贴',
                        disc: "seamless patterns for fabrics, wallpapers and textures.",
                        cinfo: '面料、壁纸和纹理的无缝图案。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'Repeat',
                        cname: '重复',
                        disc: "repeat the generation process.",
                        cinfo: '重复生成过程。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                ]
            },
            {
                tname: 'Model',
                cname: '模型',
                children: [
                    {
                        tname: 'Model',
                        cname: '模型',
                        disc: "Set the version of the Midjourney engine. In older versions faces, scenes, and creatures are more distorted, and everything is more abstract.",
                        cinfo: '设置中途发动机的版本。在旧版本中，人脸、场景和生物更加扭曲，一切都更加抽象。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                ]
            },
            {
                tname: 'Special',
                cname: '特殊设置',
                children: [
                    {
                        tname: 'Light upscaler',
                        cname: '灯光放大',
                        disc: "Upscale results will be closer to the original because the upscaler adds fewer details.",
                        cinfo: '放大后的结果将更接近原始结果，因为放大后添加的细节更少。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'test',
                        cname: '测验',
                        disc: "Use the new general purpose artistic test mode. You get two images using a square aspect ratio; or just one with a non-square aspect ratio.",
                        cinfo: '使用新的通用艺术测试模式。使用方形纵横比可以得到两张图像；或者仅仅是具有非正方形纵横比的一个。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'testp',
                        cname: '测试图片',
                        disc: "Use the new photo-realism test mode. You get two images using a square aspect ratio; or just one with a non-square aspect ratio.",
                        cinfo: '使用新的照片逼真度测试模式。使用方形纵横比可以得到两张图像；或者仅仅是具有非正方形纵横比的一个。',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'niji',
                        cname: '尼吉',
                        disc: "special model niji",
                        cinfo: '特殊型号尼吉',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                    {
                        tname: 'highdefinition',
                        cname: '高清晰度',
                        disc: "high definition ",
                        cinfo: '高清晰度',
                        pic: 'https://tevo.online/wp-content/uploads/2023/04/CTA-Home-page-New.png'
                    },
                ]
            },
        ]
    },
    //第一类目第五项Environment
    {
        tname: "Environment",
        cname: '环境',
        children: [
            {
                tname: 'background',
                cname: '背景',
                children: [
                    {
                        tname: 'moon background',
                        cname: '月球背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/648693927b5d5.jpg'
                    },
                    {
                        tname: 'airport background',
                        cname: '机场背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/64869d8b3b397.jpg'
                    },
                    {
                        tname: 'arctic backgroud',
                        cname: '北极背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/64869e5d0dc85.jpg'
                    },
                    {
                        tname: 'bay background',
                        cname: '海湾背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/64869e2d49f77.jpg'
                    },
                    {
                        tname: 'beach background',
                        cname: '海滩背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/64869f395f720.jpg'
                    },
                    {
                        tname: 'bedroom background',
                        cname: '卧室背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/64869f4f601b6.jpg'
                    },
                    {
                        tname: 'graveyard background',
                        cname: '墓地背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/64869f624570f.jpg'
                    },
                    {
                        tname: 'city background',
                        cname: '城市背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/64869f758762c.jpg'
                    },
                    {
                        tname: 'city hall background',
                        cname: '市政厅背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/64869f7f17b15.jpg'
                    },
                    {
                        tname: 'classroom background',
                        cname: '教室背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/64869f952277e.jpg'
                    },
                    {
                        tname: 'dancing hall background',
                        cname: '舞厅背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/64869fc5a6db3.jpg'
                    },
                    {
                        tname: 'desert background',
                        cname: '沙漠背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/64869fda503c6.jpg'
                    },
                    {
                        tname: 'flower field background',
                        cname: '花田背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/64869fe58b9cb.jpg'
                    },
                    {
                        tname: 'football court background',
                        cname: '足球场背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/64869ff858383.jpg'
                    },
                    {
                        tname: 'forest background',
                        cname: '森林背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a01876ac6.jpg'
                    },
                    {
                        tname: 'gallery background',
                        cname: '画廊背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a01b57008.jpg'
                    },
                    {
                        tname: 'garden background',
                        cname: '花园背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a0248acae.jpg'
                    },
                    {
                        tname: 'gym background',
                        cname: '健身房背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a02bdf128.jpg'
                    },
                    {
                        tname: 'heaven background',
                        cname: '天堂背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a03f9839b.jpg'
                    },
                    {
                        tname: 'home office background',
                        cname: '家庭办公室背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a054e7255.jpg'
                    }
                ]
            },
            {
                tname: 'background',
                cname: '更多背景',
                children: [
                    {
                        tname: 'hospital hall',
                        cname: '医院大厅',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a07ce04a3.jpg'
                    },
                    {
                        tname: 'church background',
                        cname: '教堂背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a086a1a45.jpg'
                    },
                    {
                        tname: 'inside the castle',
                        cname: '城堡内部',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a09a8ff7d.jpg'
                    },
                    {
                        tname: 'kitchen background',
                        cname: '厨房背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a0a08c6ff.jpg'
                    },
                    {
                        tname: 'library background',
                        cname: '图书馆背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a0a9a00d0.jpg'
                    },
                    {
                        tname: 'living room background',
                        cname: '客厅背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a0b3134e4.jpg'
                    },
                    {
                        tname: 'mountain background',
                        cname: '山地背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a0c738716.jpg'
                    },
                    {
                        tname: 'music stage background',
                        cname: '音乐舞台背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a0cf81a46.jpg'
                    },
                    {
                        tname: 'in the train background',
                        cname: '在列车中背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a0f50c591.jpg'
                    },
                    {
                        tname: 'opera hall background',
                        cname: '歌剧院背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a0ef32c11.jpg'
                    },
                    {
                        tname: 'park background',
                        cname: '公园背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a0fbb07c0.jpg'
                    },
                    {
                        tname: 'rain background',
                        cname: '降雨背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a10777331.jpg'
                    },
                    {
                        tname: 'street background',
                        cname: '街道背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a10eb2d7c.jpg'
                    },
                    {
                        tname: 'restaurant background',
                        cname: '餐厅背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a13fb11b6.jpg'
                    },
                    {
                        tname: 'river background',
                        cname: '河流背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a13d51c5c.jpg'
                    },
                    {
                        tname: 'school background',
                        cname: '学校背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a150be6de.jpg'
                    },
                    {
                        tname: 'snow background',
                        cname: '雪花背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a15b0749e.jpg'
                    },
                    {
                        tname: 'tunnel background',
                        cname: '隧道背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a15eab7c5.jpg'
                    },
                    {
                        tname: 'valley background',
                        cname: '山谷背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a165bdfd6.jpg'
                    },
                    {
                        tname: 'waterfall background',
                        cname: '瀑布背景',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486a17929de7.jpg'
                    },
                ]
            },
        ]
    },
    //第一类目第六项object
    {
        tname: "object",
        cname: '对象',
        children: [
            {
                tname: 'Human-like',
                cname: '类似人类的',
                children: [
                    {
                        tname: 'african',
                        cname: '非洲的',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: ''
                    },
                    {
                        tname: 'alien',
                        cname: '外星人',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b05d02096.jpg'
                    },
                    {
                        tname: 'asian',
                        cname: '亚洲的',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b070b8d85.jpg'
                    },
                    {
                        tname: 'astronaut',
                        cname: '宇航员',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b0845e13f.jpg'
                    },
                    {
                        tname: 'baby',
                        cname: '婴儿',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b08ac7178.jpg'
                    },
                    {
                        tname: 'black woman',
                        cname: '黑人妇女',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b0919a508.jpg'
                    },
                    {
                        tname: 'businessman',
                        cname: '商人',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b0a52d582.jpg'
                    },
                    {
                        tname: 'carpenter',
                        cname: '木工',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b0af840f4.jpg'
                    },
                    {
                        tname: 'chinese',
                        cname: '中国人',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b0b43c827.jpg'
                    },
                    {
                        tname: 'clown',
                        cname: '小丑',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b0bbc46be.jpg'
                    },
                    {
                        tname: 'cowboy',
                        cname: '牛仔',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b0c6d1f08.jpg'
                    },
                    {
                        tname: 'crone',
                        cname: '丑陋的老太婆',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b0cede822.jpg'
                    },
                    {
                        tname: 'dark lord',
                        cname: '黑暗领主',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b0d33b72f.jpg'
                    },
                    {
                        tname: 'devil',
                        cname: '魔王',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b0e6b3cb0.jpg'
                    },
                    {
                        tname: 'dracula',
                        cname: '德古拉',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b0fdb1224.jpg'
                    },
                    {
                        tname: 'dargon lady',
                        cname: '龙女',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b0f84f96a.jpg'
                    },
                    {
                        tname: 'blonde',
                        cname: '金发女郎',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b104ea903.jpg'
                    },
                    {
                        tname: 'fairy',
                        cname: '仙女',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b10e81724.jpg'
                    },
                    {
                        tname: 'filipina',
                        cname: '菲律宾人',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: ''
                    },
                    {
                        tname: 'french maid',
                        cname: '法式女仆',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b12283b36.jpg'
                    },
                    {
                        tname: 'geek',
                        cname: '极客',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b12c9d5dc.jpg'
                    },
                    {
                        tname: 'geisha',
                        cname: '艺妓',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b1365704c.jpg'
                    },
                    {
                        tname: 'ghost',
                        cname: '鬼魂',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b14075d9b.jpg'
                    },
                    {
                        tname: 'goddess',
                        cname: '女神',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b158c77d9.jpg'
                    },
                    {
                        tname: 'goth punk',
                        cname: '哥特朋克',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b154ce641.jpg'
                    },
                    {
                        tname: 'hipster',
                        cname: '时髦的人',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b15ea3832.jpg'
                    },
                    {
                        tname: 'hispanic',
                        cname: '西班牙的',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b1671cf12.jpg'
                    }
                ]
            },
            {
                tname: 'Human-like',
                cname: '更多类人',
                children: [
                    {
                        tname: 'housewife',
                        cname: '家庭妇女',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b170a4547.jpg'
                    },
                    {
                        tname: 'indigenous',
                        cname: '土著',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b17b142fc.jpg'
                    },
                    {
                        tname: 'journalist',
                        cname: '新闻记者',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b192423b3.jpg'
                    },
                    {
                        tname: 'kid',
                        cname: '小孩',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b18d4f096.jpg'
                    },
                    {
                        tname: 'scientist',
                        cname: '科学家',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b1a3afb3c.jpg'
                    },
                    {
                        tname: 'medusa',
                        cname: '美杜莎',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b1afd3d95.jpg'
                    },
                    {
                        tname: 'muse',
                        cname: '缪斯',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b1cbb4dfb.jpg'
                    },
                    {
                        tname: 'nerd',
                        cname: '书呆子',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b1c745be9.jpg'
                    },
                    {
                        tname: 'ninja',
                        cname: '忍者',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b1dca2acd.jpg'
                    },
                    {
                        tname: 'nun',
                        cname: '修女',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b1e701451.jpg'
                    },
                    {
                        tname: 'painter',
                        cname: '油漆匠',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b1f614348.jpg'
                    },
                    {
                        tname: 'detective',
                        cname: '侦探',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b2095209e.jpg'
                    },
                    {
                        tname: 'pirate',
                        cname: '海盗',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b212c1c02.jpg'
                    },
                    {
                        tname: 'police',
                        cname: '警察',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b21818c2c.jpg'
                    },
                    {
                        tname: 'politician',
                        cname: '政客',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b22467bb7.jpg'
                    },
                    {
                        tname: 'priest',
                        cname: '牧师',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b21e242b3.jpg'
                    },
                    {
                        tname: 'professor',
                        cname: '教授',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b2422593c.jpg'
                    },
                    {
                        tname: 'queen',
                        cname: '女王',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b247abe4a.jpg'
                    },
                    {
                        tname: 'robot',
                        cname: '机器人',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b24a2b83b.jpg'
                    },
                    {
                        tname: 'sniper',
                        cname: '狙击手',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: ''
                    },
                    {
                        tname: 'student',
                        cname: '学生',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b250eca11.jpg'
                    },
                    {
                        tname: 'spy',
                        cname: '间谍',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b277c4bb7.jpg'
                    },
                    {
                        tname: 'superhero',
                        cname: '超级英雄',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b26d7bc7f.jpg'
                    },
                    {
                        tname: 'vampire',
                        cname: '吸血鬼',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b27386626.jpg'
                    },
                    {
                        tname: 'warrior',
                        cname: '战士',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b27c626fe.jpg'
                    },
                    {
                        tname: 'werewolf',
                        cname: '狼人',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b28de52a9.jpg'
                    },
                    {
                        tname: 'witch',
                        cname: '女巫',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b293aa5f3.jpg'
                    },
                    {
                        tname: 'zombie',
                        cname: '僵尸',
                        disc: "no data info",
                        cinfo: '无数据信息',
                        pic: 'http://qnimgstore.ywfun.cn/6486b298c8d2e.jpg'
                    },
                ]
            },
        ]
    },
]