<link rel="stylesheet" href="<?= $_baseUrl ?>assets/lib/elementui.css" />
<script src="https://unpkg.com/vue@2.6.11/dist/vue.js"></script>
<script src="https://unpkg.com/element-ui@2.13.0/lib/index.js"></script>
<script src="<?= $_baseUrl ?>assets/lib/axios.js"></script>
<script src="<?= $_baseUrl ?>assets/lib/dataList.js"></script>
<style>
    .el-input__inner {
        height: 32px;
    }

    .el-input__icon {
        line-height: 35px;
        cursor: pointer;
    }

    .el-dialog__headerbtn {
        font-size: 28px;
    }

    .el-link {
        justify-content: left;
        font-size: 14px;
        line-height: 32px;
    }

    .el-link.el-link--info {
        color: #333;
    }


    .tip-box {
        background: #d8d3d3;
        height: 550px;
        display: flex;
        flex-direction: row;
    }

    .main-left {
        width: 50%;
        background: #fff;
        display: flex;
        flex-direction: row;
    }

    .main-left ::-webkit-scrollbar {
        display: none;
    }

    .actived {
        color: #007cba !important;
        background-color: #eee;
        font-weight: bolder;
        border-radius: 5px;
    }

    .main-right {
        width: 50%;
        padding: 0 20px;
        background: #fff;
        display: flex;
        flex-direction: column;
    }


    .el-dialog__footer {
        padding-top: 0;
    }
</style>

<div id="app">
    <a href="javascript:;" @click="openTip()"><i class="fa fa-edit"></i> 快捷编辑</a>

    <!-- 装配提示词 弹窗 -->
    <el-dialog :visible.sync="selectVisible" width="1300px" height="800px">
        <div style="
            display: flex;
            justify-content: space-between;
            align-items: center;
            height: 60px;
            margin-top: -10px;
          " slot="title">
            <h2 style="font-size: 22px; color: #666;font-weight: bolder;">提示词选择器</h2>
            <span style="display: flex; align-items: center">
                <el-input placeholder="搜索提示词" v-model.trim="searchKey" style="width:220px;" @change="searchTip">
                    <el-button slot="append" icon="el-icon-search" @click="searchTip"></el-button>
                </el-input>
                <span style="width: 60px; margin-left: 20px; color: #007cba; cursor: pointer;" @click="openAddTip">新增</span>
            </span>
            <span></span>
        </div>
        <!-- 分割线 -->
        <div style="border-bottom: 1px solid #ddd; margin-top: -30px;margin-bottom: 1px;"></div>

        <div class="tip-box">
            <div class="main-left">
                <!-- 第一级分类 -->
                <div style="
                border-right: 1px solid #ddd;
                display: flex;
                flex-direction: column;
                padding: 12px 5px 12px 0;
                width: 150px;
                height: 550px;
                overflow: auto;
                text-indent: 10px;
              ">
                    <el-link type="info" :underline="false" v-for="(item, index) in dataList" :class="choseTipPath[0]===index ? 'actived': ''" @click="choseFir(index)">
                        {{ item.tname}}
                    </el-link>
                </div>
                <!-- 第二级分类 -->
                <div style="
                border-right: 1px solid #ddd;
                display: flex;
                flex-direction: column;
                padding: 12px 5px;
                margin: 0 5px;
                width: 180px;
                height: 550px;
                overflow: auto;
                text-indent: 10px;
              ">
                    <el-link type="info" :underline="false" v-for="(item, index) in sectypes" :class="choseTipPath[1]===index ? 'actived': ''" @click="choseSec(index)">
                        {{ item.tname}}
                    </el-link>
                </div>
                <!-- 第三级分类 -->
                <div style="
                border-right: 1px solid #ddd;
                display: flex;
                flex-direction: column;
                padding: 12px 5px;
                width: 230px;
                height: 550px;
                overflow: auto;
                text-indent: 10px;
              ">
                    <el-link type="info" :underline="false" v-for="(item, index) in thirdatas" :class="choseTipPath[2]===index ? 'actived': ''" @click="choseThir(index)">
                        {{ item.tname}}
                    </el-link>
                </div>
            </div>

            <!-- 右侧展示栏目 -->
            <div class="main-right">
                <div class="header" style="display: flex;justify-content: space-between;align-items: center;margin-top: 20px;">
                    <span style="font-size: 15px;color: #333;">{{ choseTip.tname }}</span>
                    <div style="display: flex;justify-content: space-between;align-items: center;">
                        <span style="font-size: 15px;color: #333;">权重：</span>
                        <el-rate v-model="choseTipLeavel" text-color="#888" show-score @change="addTips"></el-rate>
                    </div>
                </div>
                <div style="color:#888;line-height: 22px;margin-top: 10px;">
                    {{ choseTip.disc }}
                </div>
                <div style="
                background: #d8d3d3;
                width: 620px;
                height: 400px;
                padding: 10px;
                margin-top: 20px;
              ">
                    <el-image style="width: 620px;height: 400px;" :src="choseTip.pic" fit="contain"></el-image>
                </div>
            </div>
        </div>

        <!-- 底部分割线 -->
        <div style="border-top: 1px solid #ddd;margin-top: 25px;"></div>

        <!-- 底部元素 -->
        <span slot="footer" style="display: flex; justify-content: space-between">
            <div>
                <el-tag v-for="(tag, index) in chosedTips" :key="tag.tname" :effect="mychoseIndex === index ? 'dark': ''" closable style="margin-right: 10px;cursor: pointer;" @close="removeTip(index)" @click="choseSetTip(tag, index)">
                    {{tag.tname + ' - ' + tag.leavel}}
                </el-tag>
            </div>
            <div>
                <el-button @click="selectVisible = false">取 消</el-button>
                <el-button type="primary" @click="confirm()">确 认</el-button>
            </div>
        </span>
    </el-dialog>

</div>

<script>
    new Vue({
        el: "#app",
        data() {
            return {
                // 控制提示词选择器弹框
                selectVisible: false,
                // 搜索提示词关键字
                searchKey: '',
                // 提示词基础数据
                dataList,
                // 选中的提示词层级路径
                choseTipPath: [0, 0, 0],
                // 选中提示词设置权重
                choseTipLeavel: 0,
                // 选中的提示词
                chosedTips: [],
                // 底部操作选中的索引
                mychoseIndex: 0
            };
        },
        computed: {
            // 二级分类
            sectypes() {
                return this.dataList[this.choseTipPath[0]].children
            },
            // 三级数据
            thirdatas() {
                return this.dataList[this.choseTipPath[0]].children[this.choseTipPath[1]].children
            },
            // 选中的提示词
            choseTip() {
                return this.dataList[this.choseTipPath[0]].children[this.choseTipPath[1]].children[this.choseTipPath[2]];
            }
        },
        methods: {
            // 展开提示词选择的弹窗
            openTip() {
                this.selectVisible = true;
            },
            // 点击一级选项
            choseFir(index) {
                this.choseTipPath = [index, 0, 0];
                // 判断是否存在已选择的位置，修改权重
                let findex = -1;
                this.chosedTips.forEach((item, index) => {
                    // console.log(JSON.stringify(item.path), JSON.stringify(this.choseTipPath));
                    if (JSON.stringify(item.path) === JSON.stringify(this.choseTipPath)) {
                        findex = index;
                    }
                })
                if (findex > -1) {
                    this.choseTipLeavel = this.chosedTips[findex].leavel;
                    this.mychoseIndex = findex;
                } else {
                    this.choseTipLeavel = 0;
                }

            },
            // 二级选择
            choseSec(index) {
                this.choseTipPath[1] = index;
                this.choseTipPath[2] = 0;
                this.choseTipPath = [...this.choseTipPath];
                // 判断是否存在已选择的位置，修改权重
                let findex = -1;
                this.chosedTips.forEach((item, index) => {
                    // console.log(JSON.stringify(item.path), JSON.stringify(this.choseTipPath));
                    if (JSON.stringify(item.path) === JSON.stringify(this.choseTipPath)) {
                        findex = index;
                    }
                })
                if (findex > -1) {
                    this.choseTipLeavel = this.chosedTips[findex].leavel;
                    this.mychoseIndex = findex;
                } else {
                    this.choseTipLeavel = 0;
                }
            },
            // 提示词选择
            choseThir(index) {
                this.choseTipPath[2] = index;
                this.choseTipPath = [...this.choseTipPath];
                // 判断是否存在已选择的位置，修改权重
                let findex = -1;
                this.chosedTips.forEach((item, index) => {
                    // console.log(JSON.stringify(item.path), JSON.stringify(this.choseTipPath));
                    if (JSON.stringify(item.path) === JSON.stringify(this.choseTipPath)) {
                        findex = index;
                    }
                })
                if (findex > -1) {
                    this.choseTipLeavel = this.chosedTips[findex].leavel;
                    this.mychoseIndex = findex;
                } else {
                    this.choseTipLeavel = 0;
                }
            },
            // 搜索提示词
            searchTip() {
                if (!this.searchKey) {
                    this.$message({
                        type: 'info',
                        message: '亲，提示词搜索不允许为空！'
                    });
                    return;
                } else {
                    for (let i = 0; i < dataList.length; i++) {
                        for (let j = 0; j < dataList[i].children.length; j++) {
                            for (let k = 0; k < dataList[i].children[j].children.length; k++) {
                                if (dataList[i].children[j].children[k].tname.toLowerCase().indexOf(this.searchKey.toLowerCase()) > -1) {
                                    this.choseTipPath = [i, j, k];
                                }
                            }
                        }
                    }
                }
            },
            // 选择了权重分数并且分数不为 0 则添加进状态选项
            addTips() {
                if (this.chosedTips.length >= 5) {
                    this.$message({
                        type: 'info',
                        message: '亲，最多只能选择 5 条提示词！'
                    });
                    return;
                }
                console.log('你选择的是：', JSON.stringify(this.choseTipPath));
                console.log('当前已选的：', this.chosedTips);
                // 判断当前操作的 提示词 是否已经存在在数组中了
                let findex = -1;
                this.chosedTips.forEach((item, index) => {
                    // console.log(JSON.stringify(item.path), JSON.stringify(this.choseTipPath));
                    if (JSON.stringify(item.path) === JSON.stringify(this.choseTipPath)) {
                        findex = index;
                    }
                })
                console.log(findex);

                if (findex > -1) {
                    this.chosedTips[findex].leavel = this.choseTipLeavel;
                    this.chosedTips = [...this.chosedTips];
                    this.mychoseIndex = findex;
                } else {
                    this.chosedTips.push({
                        ...this.choseTip,
                        // 注意：这里需要进行解构，不然的话，会造成数组异常
                        path: [...this.choseTipPath],
                        leavel: this.choseTipLeavel
                    });
                    this.mychoseIndex = this.chosedTips.length - 1;
                }
            },
            // 移除一个提示词
            removeTip(index) {
                this.chosedTips.splice(index, 1);
            },
            // 点击一个提示词，反向选择位置
            choseSetTip(tag, index) {
                this.choseTipPath = tag.path;
                this.choseTipLeavel = tag.leavel;
                this.mychoseIndex = index;
            },
            // 新增提示词弹窗操作
            openAddTip() {
                this.$message({
                    type: 'info',
                    message: '亲，功能暂未开放，敬请等待！'
                });
            },

            confirm() {
                this.selectVisible = false;
                var vals = $("#prompts_style").val().split(",").map(v => $.trim(v)).filter(v => !!v);
                if (this.choseTip.tname) {
                    vals.push(this.choseTip.tname);
                }
                $("#prompts_style").val(vals.join(","));
            }

        },
    });
</script>